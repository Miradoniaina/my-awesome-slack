import { PubSub } from 'graphql-subscriptions';
import { Redis } from 'ioredis';
import { UserInterface } from '../models/User.model';
import { AuthentificationService } from '../services/authentification/authentification.service';
import { ChannelService } from '../services/channel/channel.service';
import { MessageService } from '../services/channel/message/message.service';
import { PrivateMessageService } from '../services/privateMessage/privateMessage.service';

export interface ContextInterface {
  pubsub: PubSub;
  redis: Redis;
  currentUser?: UserInterface;
  services: {
    authentification: typeof AuthentificationService;
    channelUser: typeof ChannelService;
    message: typeof MessageService;
    privateMessage: typeof PrivateMessageService;
  };
}

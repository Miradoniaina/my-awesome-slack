import { makeSchema } from 'nexus';
import { join } from 'path';
import * as types from './types';

export const schema = makeSchema({
  types: [types],
  outputs: {
    typegen: join(__dirname, './__generated/', 'nexus-typegen.ts'), // 2
    schema: join(__dirname, '../', 'schema.graphql'), // 3
  },
  contextType: {
    module: join(__dirname, './ContextInterface.ts'),
    export: 'ContextInterface',
  },
});

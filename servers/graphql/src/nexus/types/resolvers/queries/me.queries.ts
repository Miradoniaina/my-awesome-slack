import { arg, extendType } from 'nexus';
import User from '../../../../models/User.model';

export const MeQuery = extendType({
  type: 'Query',
  definition(t) {
    t.field('me', {
      type: 'User',
      args: { input: arg({ type: 'InputLogin' }) },
      resolve: async (_, args, ctx) => {
        return User.findById(ctx.currentUser?.id).select([
          'email',
          'name',
          'phone',
          'firstName',
          'lastName',
          'userName',
          'profil',
        ]);
      },
    });
  },
});

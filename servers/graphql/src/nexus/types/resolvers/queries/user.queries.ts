import { extendType, list } from 'nexus';
import User from '../../../../models/User.model';

export const AllUserQuery = extendType({
  type: 'Query',
  definition(t) {
    t.field('allUsers', {
      type: list('User'),
      resolve: async () => {
        return User.find().select([
          'email',
          'name',
          'phone',
          'firstName',
          'lastName',
          'userName',
          'profil',
          'lastConnected',
        ]) as any;
      },
    });
  },
});

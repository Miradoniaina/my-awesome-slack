import { arg, extendType } from 'nexus';
import PrivateMessage from '../../../../../models/PrivateMessage.model';
import User from '../../../../../models/User.model';

export const privateMessagesQuery = extendType({
  type: 'Query',
  definition(t) {
    t.field('privateMessages', {
      type: 'PrivateMessageQueryPayload',
      args: { input: arg({ type: 'InputPrivateMessages' }) },
      resolve: async (_, args, ctx) => {
        const { input } = args;

        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }

        const privateUser = await User.findById(input?.privateUser).select([
          'email',
          'name',
          'phone',
          'firstName',
          'lastName',
          'userName',
          'profil',
        ]);
        if (!privateUser) {
          throw new Error('NOT_FOUND');
        }

        const privateMessages = await (
          await PrivateMessage.find({
            $and: [
              { $or: [{ sender: input?.privateUser }, { sender: +ctx.currentUser.id }] },
              { $or: [{ receiver: input?.privateUser }, { receiver: +ctx.currentUser.id }] },
            ],
          }).populate(['sender', 'receiver'])
        ).map((el: any) => {
          return {
            id: el.id,
            createdAt: el.createdAt,
            message: el.message,
            isMine: Boolean(ctx.currentUser && +el.sender.id === +ctx.currentUser.id),
            sender: el.sender,
            receiver: el.receiver,
          } as any;
        });

        return {
          id: privateUser.id,
          privateUser,
          privateMessages,
        };
      },
    });
  },
});

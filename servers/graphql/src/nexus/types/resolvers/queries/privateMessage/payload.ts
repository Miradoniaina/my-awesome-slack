import { list, objectType } from 'nexus';

export const PrivateMessageQueryPayload = objectType({
  name: 'PrivateMessageQueryPayload',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.field('privateUser', {
      type: 'User',
    });
    t.nonNull.field('privateMessages', {
      type: list('PrivateMessage'),
    });
  },
});

export const PrivateMessageItemPayload = objectType({
  name: 'PrivateMessageItemPayload',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.boolean('isMine');
    t.nonNull.string('message');
    t.nonNull.field('sender', {
      type: 'User',
    });
    t.nonNull.field('receiver', {
      type: 'User',
    });
    t.nonNull.date('createdAt');
  },
});

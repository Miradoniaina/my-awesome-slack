import { inputObjectType } from 'nexus';

export const InputPrivateMessages = inputObjectType({
  name: 'InputPrivateMessages',
  definition(t) {
    t.nonNull.int('privateUser');
  },
});

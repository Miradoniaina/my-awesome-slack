import { inputObjectType } from 'nexus';

export const InputChannelMessages = inputObjectType({
  name: 'InputChannelMessages',
  definition(t) {
    t.nonNull.int('channelId');
  },
});

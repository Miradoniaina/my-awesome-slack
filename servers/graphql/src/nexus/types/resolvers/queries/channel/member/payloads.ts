import { list, objectType } from 'nexus';

export const ChannelMemberPayload = objectType({
  name: 'ChannelMemberPayload',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.field('members', {
      type: list('User'),
    });
    t.nonNull.boolean('isOwner');
  },
});

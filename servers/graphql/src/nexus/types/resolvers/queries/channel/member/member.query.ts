import { extendType } from 'nexus';
import Channel from '../../../../../../models/Channel.model';
import ChannelMember from '../../../../../../models/ChannelMember.model';

export const ChannelMemberQuery = extendType({
  type: 'Query',
  definition(t) {
    t.field('channelMembers', {
      type: 'ChannelMemberPayload',
      args: { channelId: 'Int' },
      resolve: async (_, args, ctx) => {
        const channelId = args.channelId ? +args.channelId : 0;
        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }

        const channel = await Channel.findOne({
          _id: channelId,
        });

        if (!channel) {
          throw new Error('CHANNEL_NOT_FOUND');
        }

        const isOwner = +channel.owner === +ctx.currentUser.id;

        return {
          id: channelId,
          isOwner,
          members: (
            await ChannelMember.find({
              channel: channelId,
            }).populate('member')
          ).map((el: any) => el.member),
        } as any;
      },
    });
  },
});

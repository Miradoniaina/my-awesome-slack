import { arg, extendType } from 'nexus';
import Channel from '../../../../../../models/Channel.model';
import ChannelMessage from '../../../../../../models/ChannelMessage.model';

export const ChannelMessageQuery = extendType({
  type: 'Query',
  definition(t) {
    t.field('channelMessages', {
      type: 'ChannelMessageQueryPayload',
      args: { input: arg({ type: 'InputChannelMessages' }) },
      resolve: async (_, args) => {
        const channelId = +(args.input?.channelId || 0);
        return {
          id: channelId,
          channelMessages: await ChannelMessage.find({
            channel: channelId,
          }).populate('sender'),
          channelName: (await Channel.findById(channelId).select('name'))?.name,
        } as any;
      },
    });
  },
});

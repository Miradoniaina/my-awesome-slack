import { list, objectType } from 'nexus';

export const ChannelMessageQueryPayload = objectType({
  name: 'ChannelMessageQueryPayload',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.field('channelMessages', {
      type: list('ChannelMessage'),
    });
    t.nonNull.field('channelName', {
      type: 'String',
    });
  },
});

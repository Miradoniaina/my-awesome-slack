import { withFilter } from 'graphql-subscriptions';
import { extendType } from 'nexus';

export const PUBLISHED_SEND_PRIVATE_MESSAGE = 'PUBLISHED_SEND_PRIVATE_MESSAGE';

export const SendPrivateMessage = extendType({
  type: 'Subscription',
  definition(t) {
    t.field('sendPrivateMessage', {
      type: 'PrivateMessage',
      subscribe: withFilter(
        (_, __, ctx) => ctx.pubsub.asyncIterator(PUBLISHED_SEND_PRIVATE_MESSAGE),
        async (
          payload: {
            privateMessage: any;
          },
          __,
          ctx
        ) => {
          return +payload.privateMessage.receiver.id === +ctx.currentUser.id;
        }
      ),
      resolve(payload) {
        return payload.privateMessage;
      },
    });
  },
});

import { withFilter } from 'graphql-subscriptions';
import { extendType } from 'nexus';

export const USER_LOGS_STATUS = 'USER_LOGS_STATUS';

export const UserLogsStatus = extendType({
  type: 'Subscription',
  definition(t) {
    t.field('userLogsStatus', {
      type: 'User',
      subscribe: withFilter(
        (_, __, ctx) => ctx.pubsub.asyncIterator(USER_LOGS_STATUS),
        async (
          payload: {
            userLogged: any;
          },
          __,
          ctx
        ) => {
          return Boolean(ctx.currentUser);
        }
      ),
      resolve(payload) {
        return payload.userLogged;
      },
    });
  },
});

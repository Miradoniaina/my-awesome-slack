import { withFilter } from 'graphql-subscriptions';
import { extendType } from 'nexus';

export const PUBLISHED_ADD_CHANNEL_MESSAGE = 'PUBLISHED_ADD_CHANNEL_MESSAGE';
export const PUBLISHED_ADD_MEMBER_CHANNEL_MESSAGE = 'PUBLISHED_ADD_MEMBER';

export const AddChannelMessage = extendType({
  type: 'Subscription',
  definition(t) {
    t.field('addChannelMessage', {
      type: 'ChannelMessage',
      subscribe: withFilter(
        (_, __, ctx) => ctx.pubsub.asyncIterator(PUBLISHED_ADD_CHANNEL_MESSAGE),
        async (
          payload: {
            message: any;
          },
          __,
          ctx
        ) => {
          const isMine = payload.message.sender.id === ctx.currentUser.id;
          return !isMine;
        }
      ),
      resolve(payload) {
        return payload.message;
      },
    });
  },
});

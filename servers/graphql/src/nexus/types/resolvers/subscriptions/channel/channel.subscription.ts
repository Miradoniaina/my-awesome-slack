import { withFilter } from 'graphql-subscriptions';
import { extendType } from 'nexus';
import { ChannelMemberInterface } from '../../../../../models/ChannelMember.model';
import { UserInterface } from '../../../../../models/User.model';

export const PUBLISHED_ADD_CHANNEL = 'PUBLISHED_ADD_CHANNEL';

export const PUBLISHED_CHANNEL_USER_INVITED = 'PUBLISHED_CHANNEL_USER_INVITED';

export const PUBLISHED_CHANNEL_USER_ADD_REMOVED = 'PUBLISHED_CHANNEL_USER_ADD_REMOVED';

export const ChannelUserInvited = extendType({
  type: 'Subscription',
  definition(t) {
    t.field('userInviteChannel', {
      type: 'Channel',
      subscribe: withFilter(
        (_, __, ctx) => ctx.pubsub.asyncIterator(PUBLISHED_CHANNEL_USER_INVITED),
        async (
          payload: {
            channel: any;
            userInvitedId: number;
          },
          __,
          ctx
        ) => {
          const { userInvitedId } = payload;
          return +userInvitedId === +ctx.currentUser.id;
        }
      ),
      resolve(payload) {
        return payload.channel;
      },
    });
    t.field('userAddRemoveChannel', {
      type: 'ChannelMemberMutationPayload',
      subscribe: withFilter(
        (_, __, ctx) => ctx.pubsub.asyncIterator(PUBLISHED_CHANNEL_USER_ADD_REMOVED),
        async (
          payload: {
            channel: any;
            member: UserInterface;
            newChannelMembers: ChannelMemberInterface[];
            type: string;
          },
          __,
          ctx
        ) => {
          if (!ctx.currentUser) {
            return false;
          }

          const { member, newChannelMembers, type } = payload;

          if(type === 'add') {
            return true
          }

          return (
            +member.id === +ctx.currentUser.id ||
            newChannelMembers.some(({ member }) => +ctx.currentUser.id === +member)
          );
        }
      ),
      resolve(payload) {
        return {
          id: payload.channelId,
          member: payload.member,
          type: payload.type
        };
      },
    });
  },
});

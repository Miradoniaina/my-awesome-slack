import { inputObjectType } from 'nexus';

export const InputPrivateMessage = inputObjectType({
  name: 'InputPrivateMessage',
  definition(t) {
    t.nonNull.string('message');
    t.nonNull.string('receiverId');
  },
});

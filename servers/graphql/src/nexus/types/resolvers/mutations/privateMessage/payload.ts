import { objectType } from 'nexus';

export const SendPrivateMessageMutationPayload = objectType({
  name: 'SendPrivateMessageMutationPayload',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.field('privateUser', {
      type: 'User',
    });
    t.nonNull.field('privateMessage', {
      type: 'PrivateMessage',
    });
  },
});

import { arg, extendType, nonNull } from 'nexus';

export const PrivateMessageMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('sendPrivateMessage', {
      args: { input: nonNull(arg({ type: 'InputPrivateMessage' })) },
      type: 'PrivateMessage',
      resolve: async (_, args, ctx) => {
        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }
        return ctx.services.privateMessage.sendPrivateMessage(
          args.input,
          ctx.currentUser.id?.toString(),
          ctx.pubsub
        ) as any;
      },
    });
  },
});

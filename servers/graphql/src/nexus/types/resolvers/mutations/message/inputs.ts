import { inputObjectType } from 'nexus';

export const InputSendChannelMessage = inputObjectType({
  name: 'InputSendChannelMessage',
  definition(t) {
    t.nonNull.string('message');
    t.nonNull.string('channelId');
  },
});

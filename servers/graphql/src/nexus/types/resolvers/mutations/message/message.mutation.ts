import { arg, extendType, nonNull } from 'nexus';

export const MessageMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('sendChannelMessage', {
      args: { input: nonNull(arg({ type: 'InputSendChannelMessage' })) },
      type: 'ChannelMessage',
      resolve: async (_, args, ctx) => {
        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }
        return ctx.services.message.sendMessageChannel(
          args.input,
          ctx.currentUser.id?.toString(),
          ctx.pubsub
        ) as any;
      },
    });
  },
});

import { arg, extendType, nonNull } from 'nexus';
import Channel from '../../../../../models/Channel.model';
import ChannelMember from '../../../../../models/ChannelMember.model';
import User from '../../../../../models/User.model';
import {
  PUBLISHED_CHANNEL_USER_ADD_REMOVED,
  PUBLISHED_CHANNEL_USER_INVITED,
} from '../..';

export const ChannelUserMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('createChannelUser', {
      args: { input: nonNull(arg({ type: 'InputCreateChannelUser' })) },
      type: 'Channel',
      resolve: async (_, args, ctx) => {
        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }
        return ctx.services.channelUser.addChannel(
          args.input,
          ctx.currentUser.id?.toString()
        ) as any;
      },
    });
    t.field('addChannelParticipant', {
      args: { channel_id: 'Int', member: 'String' },
      type: 'ChannelMemberMutationPayload',
      resolve: async (_, args, ctx) => {
        const { channel_id: channelId, member } = args;

        const createMember = (
          await ChannelMember.create({
            channel: channelId,
            member,
          })
        ).populate('member');

        ctx.pubsub.publish(PUBLISHED_CHANNEL_USER_INVITED, {
          channel: await Channel.findById(channelId),
          userInvitedId: member,
        });

        ctx.pubsub.publish(PUBLISHED_CHANNEL_USER_ADD_REMOVED, {
          channelId,
          member: await User.findById(member),
          type: 'add'
        });

        return {
          id: (await createMember).channel,
          member: (await createMember).member,
        };
      },
    });
    t.field('removeChannelParticipant', {
      args: { input: nonNull(arg({ type: 'InputRemoveChannelParticipant' })) },
      type: 'ChannelMemberMutationPayload',
      resolve: async (_, { input }, ctx) => {
        const { channelId, memberId } = input;

        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }

        const channel = await Channel.findOne({
          _id: channelId,
        });

        if (!channel) {
          throw new Error('CHANNEL_NOT_FOUND');
        }
        const userToRemove = await User.findOne({
          _id: memberId,
        });

        if (!userToRemove) {
          throw new Error('USER_NOT_FOUND');
        }

        if (+channel.owner !== +ctx.currentUser.id) {
          throw new Error('NOT_AUTHORIZED');
        }

        await ChannelMember.deleteOne({
          channel: +channelId,
          member: +memberId,
        });

        const newChannelMembers = await ChannelMember.find({
          channel: channelId,
        });

        ctx.pubsub.publish(PUBLISHED_CHANNEL_USER_ADD_REMOVED, {
          channelId,
          member: userToRemove,
          newChannelMembers: newChannelMembers || [],
          type: 'remove,'
        });

        return {
          id: channelId,
          member: userToRemove,
        };
      },
    });
  },
});

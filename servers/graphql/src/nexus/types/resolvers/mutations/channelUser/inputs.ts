import { inputObjectType } from 'nexus';

export const InputCreateChannelUser = inputObjectType({
  name: 'InputCreateChannelUser',
  definition(t) {
    t.nonNull.string('channel');
    t.nonNull.boolean('private');
  },
});

export const InputRemoveChannelParticipant = inputObjectType({
  name: 'InputRemoveChannelParticipant',
  definition(t) {
    t.nonNull.id('channelId');
    t.nonNull.id('memberId');
  },
});

import { objectType } from 'nexus';

export const ChannelMemberMutationPayload = objectType({
  name: 'ChannelMemberMutationPayload',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.field('member', {
      type: 'User',
    });
    t.string('type');
  },
});

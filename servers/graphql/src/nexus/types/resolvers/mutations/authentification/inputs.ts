import { inputObjectType } from 'nexus';

export const InputLogin = inputObjectType({
  name: 'InputLogin',
  definition(t) {
    t.nonNull.string('email');
    t.nonNull.string('password');
  },
});

export const InputSignUp = inputObjectType({
  name: 'InputSignUp',
  definition(t) {
    t.nonNull.string('email');
    t.string('firstName');
    t.string('lastName');
    t.nonNull.string('phone');
    t.nonNull.string('userName');
    t.nonNull.string('password');
  },
});

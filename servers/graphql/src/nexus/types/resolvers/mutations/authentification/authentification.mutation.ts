import { arg, extendType, nonNull } from 'nexus';

export const AuthenticationMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('login', {
      args: { input: nonNull(arg({ type: 'InputLogin' })) },
      type: 'AuthPayload',
      resolve: async (_, args, ctx) => {
        return ctx.services.authentification.login(args.input) as any;
      },
    });
    t.field('signup', {
      args: { input: nonNull(arg({ type: 'InputSignUp' })) },
      type: 'AuthPayload',
      resolve: async (_, args, ctx) => {
        return ctx.services.authentification.signup(args.input) as any;
      },
    });
  },
});

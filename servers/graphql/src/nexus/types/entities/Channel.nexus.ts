import { objectType } from 'nexus';
import User from '../../../models/User.model';

export const Channel = objectType({
  name: 'Channel',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.string('name');
    t.nonNull.boolean('private');
    t.nonNull.field('owner', {
      type: 'User',
      resolve: async parent => {
        return User.findById((parent as any).owner) as any;
      },
    });
    t.nonNull.field('isOwner', {
      type: 'Boolean',
      resolve: async (parent, args, ctx) => {
        return +(ctx.currentUser?.id || 0) === +parent.id;
      },
    });
  },
});

import { objectType } from 'nexus';

export const ChannelMember = objectType({
  name: 'ChannelMember',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.date('createdAt');
  },
});

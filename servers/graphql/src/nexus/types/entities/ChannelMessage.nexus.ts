import { objectType } from 'nexus';

export const ChannelMessage = objectType({
  name: 'ChannelMessage',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.string('message');
    t.nonNull.field('createdAt', {
      type: 'DateTime',
      resolve: async parent => {
        return new Date((parent as any).createdAt);
      },
    });
    t.nonNull.field('author', {
      type: 'User',
      resolve: async parent => {
        return (parent as any).sender;
      },
    });
    t.nonNull.field('channelId', {
      type: 'String',
      resolve: async parent => {
        return (parent as any).channel;
      },
    });
    t.nonNull.field('isMine', {
      type: 'Boolean',
      resolve: async (parent, args, ctx) => {
        return Boolean(
          ctx.currentUser && (parent as any).sender.id === ctx.currentUser.id.toString()
        );
      },
    });
  },
});

import GraphQLJSON from 'graphql-type-json';
import { asNexusMethod } from 'nexus';

export const JSONType = asNexusMethod(GraphQLJSON, 'json');

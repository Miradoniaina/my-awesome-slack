import { GraphQLError, Kind } from 'graphql';
import { scalarType } from 'nexus';

export const Base64Type = scalarType({
  name: 'Base64',
  description: 'Serializes and Deserializes Base64 strings',
  serialize(value) {
    return Buffer.from(value, 'base64').toString();
  },
  parseValue(value) {
    return Buffer.from(value).toString('base64');
  },
  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Expected Base64 to be a string but got: ${ast.kind}`, [ast]);
    }
    return Buffer.from(ast.value).toString('base64');
  },
});

import { objectType } from 'nexus';

export const PrivateMessage = objectType({
  name: 'PrivateMessage',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.string('message');
    t.nonNull.date('createdAt');
    t.nonNull.field('receiver', {
      type: 'User',
    });
    t.nonNull.field('sender', {
      type: 'User',
    });
    t.nonNull.boolean('isMine', {
      resolve: async (parent, args, ctx) => {
        if (
          parent &&
          parent.sender &&
          parent.sender.id &&
          ctx &&
          ctx.currentUser &&
          ctx.currentUser.id
        ) {
          return +parent.sender.id === +ctx.currentUser.id;
        }
        return false;
      },
    });
  },
});

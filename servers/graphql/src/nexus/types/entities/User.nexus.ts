import { list, nonNull, objectType } from 'nexus';
import ChannelMember from '../../../models/ChannelMember.model';

export const User = objectType({
  name: 'User',
  definition(t) {
    t.nonNull.id('id');
    t.nonNull.string('email');
    t.nonNull.string('firstName');
    t.nonNull.string('lastName');
    t.nonNull.string('userName');
    t.string('phone');
    t.string('profil');
    t.nonNull.field('isMe', {
      type: 'Boolean',
      resolve: async (parent, args, ctx) => {
        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }
        return parent.id === ctx.currentUser.id.toString();
      },
    });
    t.nonNull.field('channels', {
      type: nonNull(list(nonNull('Channel'))),
      resolve: async (_, args, ctx) => {
        if (!ctx.currentUser) {
          throw new Error('NOT_AUTHENTICATED');
        }
        const channelMember = await ChannelMember.find({
          member: ctx.currentUser.id,
        }).populate('channel');
        return channelMember.map((el: any) => el.channel);
      },
    });
    t.field('isConnected', {
      type: 'Boolean',
      resolve: async (parent, args, ctx) => {
        const isConnectedRedis = await ctx.redis.get(`user:${parent.id}:isConnected`);
        return isConnectedRedis !== null && +isConnectedRedis === 0;
      },
    });
  },
});

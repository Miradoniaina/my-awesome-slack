import { model, Schema } from 'mongoose';
import { ChannelInterface } from './Channel.model';

export interface ChannelMessageInterface {
  message: string;
  sender: number;
  channel: number | ChannelInterface;
  createdAt: Date;
  updatedAt: Date;
}

const ChannelMessageSchema = new Schema<ChannelMessageInterface>({
  message: { type: String, required: true },
  sender: { type: Number, ref: 'user', required: true },
  channel: { type: Number, ref: 'channel', required: true },
  createdAt: { type: Date, required: true, default: new Date() },
  updatedAt: { type: Date, required: true, default: new Date() },
});

const ChannelMessage = model<ChannelMessageInterface>('channelMessage', ChannelMessageSchema);

export default ChannelMessage;

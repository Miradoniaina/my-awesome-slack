import { model, Schema } from 'mongoose';
import mongooseAutoIncrement from '../utils/mongooseAutoIncrementUtils';
import { UserInterface } from './User.model';

export interface ChannelInterface {
  _id: number;
  name: string;
  owner: string | UserInterface;
  private: boolean;
}

const ChannelSchema = new Schema<ChannelInterface>(
  {
    _id: Number,
    name: { type: String, required: true },
    owner: { type: Number, ref: 'user', required: true },
    private: { type: Boolean, default: false },
  },
  { _id: false }
);

ChannelSchema.plugin(mongooseAutoIncrement, { id: 'channel_id', inc_field: '_id' });

const Channel = model<ChannelInterface>('channel', ChannelSchema);

export default Channel;

import { model, Schema } from 'mongoose';
import { ChannelInterface } from './Channel.model';
import { UserInterface } from './User.model';

export interface ChannelMemberInterface {
  channel: number | ChannelInterface;
  member: UserInterface | UserInterface;
}

const ChannelMemberSchema = new Schema<ChannelMemberInterface>({
  channel: { type: Number, ref: 'channel', required: true },
  member: { type: Number, ref: 'user', required: true },
});

const ChannelMember = model<ChannelMemberInterface>('channelMember', ChannelMemberSchema);

export default ChannelMember;

import { model, Schema } from 'mongoose';
import mongooseAutoIncrement from '../utils/mongooseAutoIncrementUtils';

export interface UserInterface {
  id: number;
  _id: number;
  email: string;
  firstName: string;
  lastName: string;
  userName: string;
  phone?: string;
  profil?: string;
  password: string;
  createdAt: Date;
  updatedAt: Date;
}

const UserSchema = new Schema<UserInterface>(
  {
    _id: Number,
    email: { type: String, required: true, unique: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    userName: { type: String, required: true },
    phone: String,
    profil: String,
    password: String,
    createdAt: { type: Date, required: true, default: new Date() },
    updatedAt: { type: Date, required: true, default: new Date() },
  },
  { _id: false }
);

UserSchema.plugin(mongooseAutoIncrement, { id: 'user_id', inc_field: '_id' });

const User = model<UserInterface>('user', UserSchema);

export default User;

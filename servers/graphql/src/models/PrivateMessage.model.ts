import { model, Schema } from 'mongoose';
import { UserInterface } from './User.model';

export interface PrivateMessageInterface {
  message: string;
  sender: number;
  receiver: number | UserInterface;
  createdAt: Date;
  updatedAt: Date;
}

const PrivateMessageSchema = new Schema<PrivateMessageInterface>({
  message: { type: String, required: true },
  sender: { type: Number, ref: 'user', required: true },
  receiver: { type: Number, ref: 'user', required: true },
  createdAt: { type: Date, required: true, default: new Date() },
  updatedAt: { type: Date, required: true, default: new Date() },
});

const PrivateMessage = model<PrivateMessageInterface>('PrivateMessage', PrivateMessageSchema);

export default PrivateMessage;

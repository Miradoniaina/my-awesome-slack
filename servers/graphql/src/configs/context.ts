import { Context } from 'apollo-server-core';
import { ExpressContext } from 'apollo-server-express';
import { PubSub } from 'graphql-subscriptions';
import IORedis from 'ioredis';
import { verify } from 'jsonwebtoken';
import User from '../models/User.model';
import { ContextInterface } from '../nexus/ContextInterface';
import { AuthentificationService } from '../services/authentification/authentification.service';
import { ChannelService } from '../services/channel/channel.service';
import { MessageService } from '../services/channel/message/message.service';
import { PrivateMessageService } from '../services/privateMessage/privateMessage.service';
import config from '.';

export const getCurrentUser = async (token: string): Promise<any | null> => {
  if (token.length) {
    const accessToken = token.replace('Bearer ', '');
    const verifiedUser = verify(accessToken, `${config.jwt.privateKey}`) as any;

    if (verifiedUser) {
      const { email } = verifiedUser;
      const user = await User.findOne({
        email,
      }).select(['id', 'email', 'name']);
      return user;
    }
  }
  return null;
};

export const createContext = async (
  { req }: ExpressContext,
  redis: IORedis.Redis,
  pubsub: PubSub
): Promise<Context<ContextInterface>> => {
  const currentUser =
    req && req.headers && req.headers.authorization
      ? await getCurrentUser(`${req.headers.authorization}`)
      : null;

  return {
    pubsub,
    redis,
    currentUser,
    services: {
      authentification: AuthentificationService,
      channelUser: ChannelService,
      message: MessageService,
      privateMessage: PrivateMessageService,
    },
  };
};

export const createSubContext = async (
  connectionParams: any,
  redis: IORedis.Redis,
  pubsub: PubSub
): Promise<Context> => {
  const currentUser =
    connectionParams && connectionParams.authToken
      ? await getCurrentUser(`${connectionParams.authToken}`)
      : null;

  return {
    pubsub,
    redis,
    currentUser,
    services: {
      channelUser: ChannelService,
      message: MessageService,
      privateMessage: PrivateMessageService,
    },
  };
};

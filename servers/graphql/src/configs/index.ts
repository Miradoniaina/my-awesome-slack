const config = {
  jwt: {
    privateKey: process.env.JWT_PRIVATE_KEY,
  },
  mongoDb: {
    url: process.env.MONGODB_URL,
    user: process.env.MONGODB_DB_USER,
    password: process.env.MONGODB_DB_PASSWORD,
    databaseName: process.env.MONGODB_DB_NAME,
    host: process.env.MONGO_HOST,
    port: process.env.MONGO_PORT,
  },
};

export default config;

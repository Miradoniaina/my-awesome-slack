import mongoose from 'mongoose';
import config from '.';

export const getMongooseConnection = async (): Promise<typeof import('mongoose')> => {
  const { databaseName, user, password, host, port } = config.mongoDb;

  return mongoose.connect(`mongodb://${user}:${password}@${host}:${port}/${databaseName}`);
};

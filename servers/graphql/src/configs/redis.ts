import Redis from 'ioredis';

export const configureRedis = (): {
  redis: Redis.Redis;
} => {
  const options: Redis.RedisOptions = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT ? +process.env.REDIS_PORT : 6379,
    retryStrategy: (times: number) => {
      // reconnect after
      return Math.min(times * 50, 2000);
    },
    password: process.env.REDIS_PASSWORD,
  };

  const redis = new Redis(options);

  return { redis };
};

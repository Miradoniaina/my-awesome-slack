import { model, Schema } from 'mongoose';

const CounterSchema = new Schema({
  _id: {
    type: String,
    required: true,
  },
  seq: {
    type: Number,
    required: true,
  },
});

const Counter = model('Counter', CounterSchema);

const getSequenceNextValue = (seqName: any) => {
  return new Promise((resolve, reject) => {
    Counter.findByIdAndUpdate({ _id: seqName }, { $inc: { seq: 1 } }, (error, counter) => {
      if (error) {
        reject(error);
      }
      if (counter) {
        resolve(counter.seq + 1);
      } else {
        resolve(null);
      }
    });
  });
};

const insertCounter = (seqName: any) => {
  const newCounter = new Counter({ _id: seqName, seq: 1 });
  return new Promise((resolve, reject) => {
    newCounter
      .save()
      .then((data: any) => {
        resolve(data.seq);
      })
      .catch((err: any) => reject(err));
  });
};

export default {
  Counter,
  getSequenceNextValue,
  insertCounter,
};

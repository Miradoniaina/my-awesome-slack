/* eslint-disable @typescript-eslint/no-var-requires */
// tslint:disable-next-line: no-var-requires
const mongoose = require('mongoose');
// tslint:disable-next-line: no-var-requires
const MongooseAutoIncrement = require('mongoose-sequence')(mongoose);

export default MongooseAutoIncrement;

/* eslint-disable no-console */
import { ApolloServer } from 'apollo-server-express';
import cors from 'cors';
import express from 'express';
import { execute, subscribe } from 'graphql';
import { applyMiddleware } from 'graphql-middleware';
import { PubSub } from 'graphql-subscriptions';
import http from 'http';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { createContext, createSubContext, getCurrentUser } from './configs/context';
import { getMongooseConnection } from './configs/mongoose';
import { configureRedis } from './configs/redis';
import { ContextInterface } from './nexus/ContextInterface';
import { schema as schemaNexus } from './nexus/schema';
import { USER_LOGS_STATUS } from './nexus/types';

async function startApolloServer() {
  const app = express();
  const pubsub = new PubSub();
  const { redis } = configureRedis();

  app.use(cors());

  app.use(express.json());
  app.use(
    express.urlencoded({
      extended: true,
    })
  );

  // Routes
  app.get('/heathcheck', (req, res) => {
    res.status(200).send('heath check OK');
  });

  const httpServer = http.createServer(app);

  const schema = applyMiddleware(schemaNexus);

  const server = new ApolloServer({
    context: req => createContext(req, redis, pubsub),
    schema,
    plugins: [
      {
        async serverWillStart() {
          return {
            async drainServer() {
              subscriptionServer.close();
            },
          };
        },
      },
    ],
  });

  const subscriptionServer = SubscriptionServer.create(
    {
      schema,
      execute,
      subscribe,
      onConnect: async (connectionParams: any) => {
        const ctx = await createSubContext(connectionParams, redis, pubsub);

        if (connectionParams && connectionParams.authToken) {
          const { authToken } = connectionParams;
          if (!authToken) {
            return {};
          }
          const currentUser = await getCurrentUser(authToken);
          await redis.set(`user:${currentUser.id}:isConnected`, 0);

          pubsub.publish(USER_LOGS_STATUS, {
            userLogged: currentUser,
          });

          return {
            ...ctx,
            currentUser,
          };
        }
        return ctx;
      },
      onDisconnect: async (webSocket: any, context: any) => {
        const initialContext: ContextInterface | null = await context.initPromise;

        if (
          initialContext &&
          typeof initialContext === 'object' &&
          Reflect.has(initialContext, 'currentUser')
        ) {
          const { currentUser } = initialContext;
          if (currentUser) {
            await redis.set(`user:${currentUser.id}:isConnected`, 1);

            pubsub.publish(USER_LOGS_STATUS, {
              userLogged: currentUser,
            });

            return {
              ...initialContext,
              currentUser,
            };
          }
        }
        return {};
      },
    },
    { server: httpServer, path: server.graphqlPath }
  );

  await server.start();
  server.applyMiddleware({
    app,
    path: '/',
    cors: false,
  });

  const PORT = 4000;
  httpServer.listen(PORT, () => {
    console.log(`🚀 Server ready at http://localhost:${PORT}/graphql`);
    console.log(`🚀 Subscription ready at: ws://localhost:${PORT}/graphql\n⭐️ `);
  });
}

getMongooseConnection()
  .then(() => {
    startApolloServer();
  })
  .catch(e => {
    // eslint-disable-next-line no-console
    console.error(e);
  });

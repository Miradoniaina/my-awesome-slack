import { PubSub } from 'graphql-subscriptions';
import PrivateMessageModel from '../../models/PrivateMessage.model';
import User from '../../models/User.model';
import { NexusGenInputs } from '../../nexus/__generated/nexus-typegen';
import { PUBLISHED_SEND_PRIVATE_MESSAGE } from '../../nexus/types/resolvers/subscriptions/privateMessage/privateMessage.subscription';

export const PrivateMessageService = {
  async sendPrivateMessage(
    input: NexusGenInputs['InputPrivateMessage'],
    senderId: string,
    pubsub: PubSub
  ) {
    const { message, receiverId: receiver } = input;

    const createdPrivateMessage = await (
      await PrivateMessageModel.create({
        message,
        sender: senderId,
        receiver,
        createdAt: new Date(),
      })
    ).populate(['sender', 'receiver']);

    pubsub.publish(PUBLISHED_SEND_PRIVATE_MESSAGE, {
      privateMessage: createdPrivateMessage,
    });

    return createdPrivateMessage as any;
  },
  async getPrivateMessages(privateUser: number, userId: number) {
    const findPrivateUser = await User.findById(2).select([
      'email',
      'name',
      'phone',
      'firstName',
      'lastName',
      'userName',
      'profil',
    ]);

    if (!findPrivateUser) {
      throw new Error('NOT_FOUND');
    }

    const privateMessages = await (
      await PrivateMessageModel.find({
        $and: [
          { $or: [{ sender: privateUser }, { sender: +userId }] },
          { $or: [{ receiver: privateUser }, { receiver: +userId }] },
        ],
      }).populate(['sender', 'receiver'])
    ).map((el: any) => {
      return {
        id: el.id,
        createdAt: el.createdAt,
        message: el.message,
        isMine: true,
        sender: el.sender,
        receiver: el.receiver,
      };
    });

    return {
      id: findPrivateUser.id,
      privateUser,
      privateMessages,
    } as any;
  },
};

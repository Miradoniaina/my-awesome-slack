import { PubSub } from 'graphql-subscriptions';
import ChannelMessage from '../../../models/ChannelMessage.model';
import { NexusGenInputs } from '../../../nexus/__generated/nexus-typegen';
import { PUBLISHED_ADD_CHANNEL_MESSAGE } from '../../../nexus/types';

export const MessageService = {
  async sendMessageChannel(
    input: NexusGenInputs['InputSendChannelMessage'],
    userId: string,
    pubsub: PubSub
  ) {
    const { message, channelId } = input;

    const createdChannelMessage = await ChannelMessage.create({
      message,
      sender: userId,
      channel: +channelId,
      createdAt: new Date(),
    });

    const populatedChannelMessage = await ChannelMessage.findById(
      createdChannelMessage.id
    ).populate('sender');

    pubsub.publish(PUBLISHED_ADD_CHANNEL_MESSAGE, {
      message: populatedChannelMessage,
    });

    return populatedChannelMessage as any;
  },
};

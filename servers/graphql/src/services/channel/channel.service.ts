import ChannelModel from '../../models/Channel.model';
import ChannelMember from '../../models/ChannelMember.model';
import { NexusGenInputs } from '../../nexus/__generated/nexus-typegen';

export const ChannelService = {
  async addChannel(input: NexusGenInputs['InputCreateChannelUser'], userId: string) {
    const { channel } = input;

    const channelCreated = await ChannelModel.create({
      name: channel,
      owner: userId,
      private: input.private,
    });

    ChannelMember.create({
      channel: channelCreated.id,
      member: userId,
    });

    return channelCreated as any;
  },
};

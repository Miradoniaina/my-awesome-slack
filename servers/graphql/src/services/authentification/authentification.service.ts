import { compare, hash } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import config from '../../configs';
import User from '../../models/User.model';
import { NexusGenInputs } from '../../nexus/__generated/nexus-typegen';

export const AuthentificationService = {
  async login(input: NexusGenInputs['InputLogin']) {
    const { email, password } = input;

    if (email && password) {
      const user = await User.findOne({
        email,
      });

      if (user && (await compare(password.trim(), user.password))) {
        return {
          accessToken: sign(
            {
              email: user.email,
            },
            `${config.jwt.privateKey}`
          ),
        } as any;
      }
    }
    throw new Error('LOGIN_ERROR');
  },
  async signup(input: NexusGenInputs['InputSignUp']) {
    const { email, password, phone, firstName, lastName, userName } = input;

    if (email && password) {
      const user = new User({
        email,
        password: await hash(password, 10),
        phone,
        firstName,
        lastName,
        userName,
      });

      user.save();

      return {
        accessToken: sign(
          {
            email,
            firstName,
            lastName,
          },
          `${config.jwt.privateKey}`
        ),
      };
    }
  },
};

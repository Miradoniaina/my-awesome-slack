

conn = new Mongo();
db = conn.getDB("myapp");

db.createUser({
  user: 'mongouser',
  pwd: 'aEcyA4c3Wq3YWfeU',
  roles: [
    {
      role: 'root',
      db: 'admin',
    },
  ],
});

db.users.createIndex({ email: 1 }, { unique: true });
db.users.insertMany([
  { _id: 1, email: "miradoeddy@gmail.com", firstName: "Eddy", lastName: 'Miradoniaina', userName: 'Miradoniaina', profil: 'https://images.unsplash.com/photo-1621182203284-900d18663eb2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80', phone: "(261) 32 43 345 44", password: "$2b$10$iwr/Z7jNbzlXFX.2Uvr86e2Kq5xuHiM4hF6TOwgsOYIPKbCqJwIB6" },
  { _id: 2, email: "viovar@gmail.com", firstName: "Viovar", lastName: 'Route', userName: 'Viovar99', profil: 'https://images.unsplash.com/photo-1524250502761-1ac6f2e30d43?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=688&q=80', phone: "(804) 637-4095", password: "$2b$10$iwr/Z7jNbzlXFX.2Uvr86e2Kq5xuHiM4hF6TOwgsOYIPKbCqJwIB6" },
  { _id: 3, email: "shady@gmail.com", firstName: "Shady", lastName: 'Footpath', userName: 'Shady08', profil: 'https://images.unsplash.com/photo-1524511498335-b0ed6b8e9a69?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1513&q=80', phone: "(854) 637-4165", password: "$2b$10$iwr/Z7jNbzlXFX.2Uvr86e2Kq5xuHiM4hF6TOwgsOYIPKbCqJwIB6" },
  { _id: 4, email: "cleora@gmail.com", firstName: "Rau", lastName: 'Cleora33', userName: 'Helga02', profil: 'https://images.unsplash.com/photo-1535295972055-1c762f4483e5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80', phone: "(854) 637-4199", password: "$2b$10$iwr/Z7jNbzlXFX.2Uvr86e2Kq5xuHiM4hF6TOwgsOYIPKbCqJwIB6" }
])


## Name
MY-AWESOME-SLACK

## Author

👤 **RABETSIMANDRANTO Miradoniaina Eddy**

## Preview
<img src="capture.png" />

## Description
A clone of Slack to communicate with teams. Built with

- ReactJs
- Material-UI
- Apollo server express 
- Apollo client
- Nexus - Graphql
- Docker
- Mongoose
- MongoDb


## Installation
Copy servers/.env.example file to servers/.env:

```sh
cp servers/.env.example servers/.env && cp clients/my-awesome-slack-react-app/.env.example clients/my-awesome-slack-react-app/.env
```

Build, start the server in detach mode and watch graphql container log

```sh
cd servers && docker-compose up --build -d && docker-compose logs -f graphql
```

Install dependencies and start the client :
```sh
cd clients/my-awesome-slack-react-app && yarn && yarn start
```
## Client User Access: 
```python
#  Mirado 
    -   email: miradoeddy@gmail.com 
    -   password: password

#  Viova 
    -  email: viovar@gmail.com
    -  password: password

#  Shady 
    -  email: shady@gmail.com
    -  password: password

# Cleora
    -  email: cleora@gmail.com
    -  password: password
```
const colors = {
  white: '#FFFFFF',
  black: '#000000',
  red: '#FF0000',
  gray: '#BDBDBD',
  pink: '#E91E63',
  grape: '#1A1A40',
  codGray: '#1d1d1d',
  toryBlue: '#270082',
  brilliantRose: '#FA58B6',
  valentino: '#1A1A40',
  jungleGreen: '#2BAC76',
  transparent: 'transparent',
  apple: '#4BB543',
  orange: 'orange',
};

export default colors;

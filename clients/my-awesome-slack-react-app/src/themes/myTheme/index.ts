import { createTheme } from '@material-ui/core';
import { boxShadow } from './boxShadows';
import colors from './colors';
import { containerStyles } from './containerStyles';
import { fontFamily } from './fontFamily';
import { textStyles } from './textStyles';

const myTheme = createTheme({
  textStyles,
  containerStyles,
  colors,
  fontFamily,
  boxShadow,
  palette: {
    primary: {
      main: colors.grape,
    },
  },
  overrides: {
    MuiCircularProgress: {
      colorSecondary: {
        color: colors.brilliantRose,
      },
    },
    MuiTypography: {
      colorTextPrimary: {
        color: colors.black,
      },
      colorTextSecondary: {
        color: colors.white,
      },
      colorSecondary: {
        color: colors.toryBlue,
      },
      colorInherit: {
        color: colors.orange,
      },
    },
  },
});

export default myTheme;

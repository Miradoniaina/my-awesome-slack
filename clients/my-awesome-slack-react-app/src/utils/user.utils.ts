import { UserBasicInfosFragment } from '../generated/graphql';

const toLocalUppercase = (str: string) => `${str.substr(0, 1).toUpperCase()}${str.substr(1)}`;

export const getUserName = (user?: UserBasicInfosFragment | null) => {
  if (user) {
    const { firstName, lastName } = user;
    return `${toLocalUppercase(firstName)} ${toLocalUppercase(lastName)}`;
  }
  return '';
};

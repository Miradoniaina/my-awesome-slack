import { Grid, Typography } from '@material-ui/core';
import classnames from 'classnames';
import { FC } from 'react';
import { SlackAuthenticationDetails } from './SlackAuthenticationDetails';
import { SlackLoginForm } from './SlackLoginForm';
import useStyle from './styles';

const Authentication: FC = () => {
  const classes = useStyle();

  return (
    <div className={classes.loginPageContainer}>
      <Grid container={true} lg={8} item={true} sm={12} className={classes.panelContainer}>
        <Grid container={true}>
          <Grid
            item={true}
            sm={7}
            xs={12}
            className={classnames(classes.gridItemContainer, classes.gridItemLeftContainer)}
          >
            <Typography color="secondary" variant="h2">
              Sign in
            </Typography>
            <SlackLoginForm />
          </Grid>
          <SlackAuthenticationDetails className={classes.gridItemContainer} />
        </Grid>
      </Grid>
    </div>
  );
};

export default Authentication;

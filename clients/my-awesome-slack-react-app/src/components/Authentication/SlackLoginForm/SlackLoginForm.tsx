import { TextField } from '@material-ui/core';
import { KeyboardEvent, useState } from 'react';
import { CustomButton } from '../../../common/CustomButton';
import { useLoginMutation } from '../../../generated/graphql';
import authService from '../../../services/authServices';
import useStyle from './styles';

const SlackLoginForm = () => {
  const classes = useStyle();

  const [email, setEmail] = useState<string | null>('');
  const [password, setPassword] = useState<string | null>('');
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const [errorLog, setErrorLog] = useState('');

  const [loginMutation, { loading: isLoading }] = useLoginMutation();

  const handleLogin = () => {
    if (!isLoading && email?.trim() && password?.length) {
      setErrorLog('');
      loginMutation({
        variables: {
          input: {
            password,
            email: email.trim(),
          },
        },
      })
        .then(resp => {
          if (resp.data?.login?.accessToken) {
            authService.setAccessToken(resp.data?.login?.accessToken);
            setIsLoggedIn(true);
          } else {
            setErrorLog('Incorrect login or password');
          }
        })
        .catch(error => {
          if (error.networkError) {
            setErrorLog('Network error');
          } else if (error.graphQLErrors.length) {
            setErrorLog('Internal server error');
          }
        });
    }
  };

  const handleEnterPress = (evt: KeyboardEvent<HTMLDivElement>) => {
    if (evt.key === 'Enter') {
      handleLogin();
    }
  };

  if (isLoggedIn) {
    window.location.href = '/';
    return null;
  }

  return (
    <div className={classes.containerSlackLoginForm} onKeyDown={handleEnterPress}>
      <form onSubmit={handleLogin}>
        <div>
          <TextField
            onChange={evt => setEmail(evt.target.value || null)}
            value={email || ''}
            error={email === undefined}
            className={classes.inputText}
            placeholder="Email"
            variant="filled"
          />
          <TextField
            onChange={evt => setPassword(evt.target.value || null)}
            value={password || ''}
            error={password === undefined}
            className={classes.inputText}
            type="password"
            placeholder="Password"
            variant="filled"
            autoComplete=""
          />
        </div>
        <h5 className={classes.errorMessage}>{errorLog}</h5>
        <CustomButton
          onClick={handleLogin}
          className={classes.buttonSubmit}
          disabled={!password || !email}
          isLoading={isLoading}
        >
          Sign in
        </CustomButton>
      </form>
    </div>
  );
};

export default SlackLoginForm;

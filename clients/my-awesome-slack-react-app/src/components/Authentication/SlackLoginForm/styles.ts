import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerSlackLoginForm: {
    marginTop: 50,
    [theme.breakpoints.down('xs')]: {
      marginTop: 90,
    },
  },
  inputText: {
    width: '100%',
    color: theme.colors.black,
    '&:last-child': {
      [theme.breakpoints.down('xs')]: {
        marginTop: 45,
      },
      marginTop: 64,
    },
  },
  buttonSubmit: {
    marginTop: 30,
    width: '100%',
    textTransform: 'none',
    paddingBottom: 10,
    paddingTop: 10,
    [theme.breakpoints.down('xs')]: {
      marginTop: 77,
    },
  },
  errorMessage: {
    margin: 0,
    marginTop: 50,
    fontSize: 14,
    fontWeight: 'normal',
    color: theme.colors.red,
    minHeight: 30,
  },
}));

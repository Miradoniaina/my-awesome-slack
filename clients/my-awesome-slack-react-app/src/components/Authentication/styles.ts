import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  loginPageContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh',
  },
  panelContainer: {
    boxShadow: theme.boxShadow.main,
    maxWidth: 1291,
  },
  gridItemContainer: {
    height: 654,
    position: 'relative',
    [theme.breakpoints.down('xs')]: {
      height: 'auto',
      paddingBottom: 35,
    },
  },
  gridItemLeftContainer: {
    padding: '86px 120px',
    [theme.breakpoints.down('sm')]: {
      padding: '35px 35px 35px 35px',
    },
  },
  gridItemRightContainer: {
    position: 'relative',
  },
}));

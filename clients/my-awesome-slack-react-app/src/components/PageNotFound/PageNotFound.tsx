import { FC } from 'react';

const PageNotFound: FC = () => {
  return (
    <h1
      style={{
        textAlign: 'center',
      }}
    >
      404 Not found
    </h1>
  );
};

export default PageNotFound;

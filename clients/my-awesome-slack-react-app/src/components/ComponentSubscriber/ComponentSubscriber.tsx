import { FC } from 'react';
import { useHistory } from 'react-router-dom';
import {
  useSubscribeToChannelMessageAddedSubscription,
  useSubscribeToUserAddedRemovedInChannelSubscription,
} from '../../generated/graphql';
import { updateCacheAfterSubscribsriptionMessageAdded } from '../../updateCache/channel/subscriptionMessageAdded';
import { updateCacheAfterSubscriptionUserChannelAddedRemoved } from '../../updateCache/channel/subscriptionUserChannelRemoved';

const ComponentSubscriber: FC = () => {
  const history = useHistory();
  useSubscribeToUserAddedRemovedInChannelSubscription({
    onSubscriptionData: updateCacheAfterSubscriptionUserChannelAddedRemoved(history),
  });

  useSubscribeToChannelMessageAddedSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionMessageAdded,
  });
  return null;
};

export default ComponentSubscriber;

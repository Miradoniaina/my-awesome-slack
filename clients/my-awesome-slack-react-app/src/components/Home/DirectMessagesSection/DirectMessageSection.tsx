import { List, Typography } from '@material-ui/core';
import { FC } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { DirectMessageMenuItem } from '../../../common/DirectMessageMenuItem';
import { Skeleton } from '../../../common/Skeleton';
import { useGetAllUserQuery } from '../../../generated/graphql';
import { isChannelFn } from '../../../utils/locationUtils';
import { getUserName } from '../../../utils/user.utils';
import useStyle from './styles';

const DirectMessagesSection: FC = () => {
  const classes = useStyle();

  const { id } = useParams<{ id: string }>();
  const { pathname } = useLocation();

  const isChannel = isChannelFn(pathname);

  const { data, loading, error } = useGetAllUserQuery();

  const users = data?.allUsers || [];

  return (
    <div className={classes.containerDirectMessagesSection}>
      <Typography className={classes.title} color="textSecondary">
        Direct messages
      </Typography>
      <List>
        {error && <Typography color="error">Error</Typography>}
        {loading && (
          <>
            <Skeleton height={35} />
            <Skeleton height={35} />
            <Skeleton height={35} />
          </>
        )}
        {!loading &&
          users.map(user =>
            user?.isMe ? null : (
              <DirectMessageMenuItem
                isActive={!isChannel && ((user && user.id === id) || false)}
                userName={getUserName(user)}
                userId={user && user.id ? user.id : ''}
                key={user && user.id}
                isOnline={!!user?.isConnected}
              />
            )
          )}
      </List>
    </div>
  );
};

export default DirectMessagesSection;

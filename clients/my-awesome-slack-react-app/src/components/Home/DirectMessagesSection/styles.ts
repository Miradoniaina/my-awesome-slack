import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerDirectMessagesSection: {
    marginTop: 35,
  },
  title: {
    paddingLeft: 14,
  },
}));

import { FC } from 'react';
import { ErrorComponent } from '../../common/ErrorComponent';
import Loading from '../../common/Loading';
import { useMeQuery, useSubscribeToUserLogsStatusSubscription } from '../../generated/graphql';
import { getUserName } from '../../utils/user.utils';
import { ComponentSubscriber } from '../ComponentSubscriber';
import Center from './Center';
import { Header } from './Header';
import { Left } from './Left';
import useStyle from './styles';

const Home: FC = () => {
  const classes = useStyle();
  const { data, loading, error } = useMeQuery();

  useSubscribeToUserLogsStatusSubscription();

  if (error || loading) {
    return (
      <div className={classes.containerHome}>
        {!loading && error && <ErrorComponent error={error} isFullWidth={true} />}
        {loading && <Loading isFullHeight={true} />}
      </div>
    );
  }
  const user = data?.me;

  if (!user) {
    return null;
  }

  return (
    <div className={classes.containerHome}>
      <Header userFullName={getUserName(user)} profil={user && user.profil ? user.profil : ''} />
      <div className={classes.containerHomeBody}>
        <Left />
        <Center />
      </div>
      <ComponentSubscriber />
    </div>
  );
};

export default Home;

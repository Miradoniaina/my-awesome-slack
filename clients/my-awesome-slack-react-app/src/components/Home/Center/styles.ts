import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerCenter: {
    flex: 1,
    backgroundColor: theme.colors.white,
    ...theme.containerStyles.flexColumn,
  },
}));

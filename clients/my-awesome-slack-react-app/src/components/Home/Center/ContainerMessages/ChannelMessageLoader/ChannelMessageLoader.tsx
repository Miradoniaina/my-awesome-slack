import { FC } from 'react';
import { ErrorComponent } from '../../../../../common/ErrorComponent';
import Loading from '../../../../../common/Loading';
import {
  ChannelMessagesBasicInfosFragment,
  useGetChannelMessagesQuery,
} from '../../../../../generated/graphql';
import { Discussion } from '../Discussion';
import useStyle from './styles';

interface ChanelMessageLoaderProps {
  channelId: number;
}

const ChanelMessageLoader: FC<ChanelMessageLoaderProps> = props => {
  const { channelId } = props;

  const { data, loading, error } = useGetChannelMessagesQuery({
    variables: {
      input: {
        channelId,
      },
    },
  });

  const channelName = data?.channelMessages?.channelName;
  const channelData = (data && data.channelMessages && data.channelMessages.channelMessages) as
    | ChannelMessagesBasicInfosFragment[]
    | null
    | undefined;

  const classes = useStyle();

  return (
    <div className={classes.containerChannelMessageLoader}>
      {loading && <Loading isFullHeight={true} />}
      {!loading && error && <ErrorComponent error={error} isFullWidth={true} />}
      {!loading && !error && channelName && (
        <Discussion isOnline={false} data={channelData} isChannel={true} title={channelName} />
      )}
    </div>
  );
};

export default ChanelMessageLoader;

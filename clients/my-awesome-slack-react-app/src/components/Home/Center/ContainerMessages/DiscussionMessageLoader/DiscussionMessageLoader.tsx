import { FC } from 'react';
import { ErrorComponent } from '../../../../../common/ErrorComponent';
import Loading from '../../../../../common/Loading';
import {
  PrivateMessageItemInfosFragment,
  useGetPrivateMessageQuery,
  useSubscribeToPrivateMessageAddedSubscription,
} from '../../../../../generated/graphql';
import { updateCacheAfterSubscribsriptionPrivateMessageAdded } from '../../../../../updateCache/channel/subscriptionPrivateMessageAdded';
import { getUserName } from '../../../../../utils/user.utils';
import { Discussion } from '../Discussion';
import useStyle from './styles';

interface DiscussionMessageLoaderProps {
  userId: number;
}

const DiscussionMessageLoader: FC<DiscussionMessageLoaderProps> = props => {
  const { userId } = props;

  useSubscribeToPrivateMessageAddedSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionPrivateMessageAdded,
  });

  const { data, loading, error } = useGetPrivateMessageQuery({
    variables: {
      input: {
        privateUser: +userId,
      },
    },
  });

  const privateMessageData = (data &&
    data.privateMessages &&
    data.privateMessages.privateMessages) as PrivateMessageItemInfosFragment[] | null | undefined;
  const classes = useStyle();
  return (
    <div className={classes.containerDiscussionMessageLoader}>
      {loading && <Loading isFullHeight={true} />}
      {error && <ErrorComponent error={error} isFullWidth={true} />}
      {!loading && !error && (
        <Discussion
          isOnline={false}
          data={privateMessageData}
          isChannel={false}
          profil={data?.privateMessages?.privateUser.profil}
          title={getUserName(data?.privateMessages?.privateUser)}
        />
      )}
    </div>
  );
};

export default DiscussionMessageLoader;

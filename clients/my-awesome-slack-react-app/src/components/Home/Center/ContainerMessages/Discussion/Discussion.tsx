import { FC } from 'react';
import { ListMessage } from '../../../../../common/ListMessage';
import {
  ChannelMessagesBasicInfosFragment,
  PrivateMessageItemInfosFragment,
} from '../../../../../generated/graphql';
import { MessageBox } from '../../MessageBox';
import useStyle from './styles';
import { TitleMessage } from './TitleMessage';

interface DiscussionProps {
  title: string;
  isChannel: boolean;
  data: (PrivateMessageItemInfosFragment | ChannelMessagesBasicInfosFragment)[] | null | undefined;
  isOnline: boolean;
  profil?: string | null;
}

const Discussion: FC<DiscussionProps> = props => {
  const { title, isChannel, isOnline, data, profil } = props;

  const classes = useStyle();
  return (
    <div className={classes.containerDiscussion}>
      <TitleMessage isOnline={isOnline} title={title} profil={profil} isChannel={isChannel} />
      <ListMessage data={data} isChannel={isChannel} />
      <MessageBox />
    </div>
  );
};

Discussion.defaultProps = {
  profil: '',
};

export default Discussion;

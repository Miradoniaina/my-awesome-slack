import makeStyles from '@material-ui/core/styles/makeStyles';
import colors from '../../../../../../../themes/myTheme/colors';

export default makeStyles(() => ({
  containerChannelParticipants: {
    marginLeft: 'auto',
  },
  addParticipant: {
    width: 50,
    height: 50,
    color: colors.brilliantRose,
  },
}));

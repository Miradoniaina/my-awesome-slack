import { Divider, Typography } from '@material-ui/core';
import { FC } from 'react';
import { useParams } from 'react-router-dom';
import { Avatar } from '../../../../../../common/Avatar';
import { ChannelParticipants } from './ChannelParticipants';
import useStyle from './styles';

interface TitleMessageProps {
  title: string;
  isChannel: boolean;
  isOnline: boolean;
  profil?: string | null;
}

const TitleMessage: FC<TitleMessageProps> = props => {
  const { title, isChannel, isOnline, profil } = props;
  const classes = useStyle();
  const { id } = useParams<{ id: string }>();

  return (
    <div className={classes.containerTitleMessage}>
      <div className={classes.titleContainer}>
        {!isChannel && profil && (
          <Avatar
            indicatorStatus={isOnline ? 'online' : 'offline'}
            size={35}
            userName={`${title}`}
            isCircle={true}
            profil={profil}
          />
        )}
        <Typography variant="h5">
          {isChannel && '#'}
          <span className={classes.title}>{title}</span>
        </Typography>
        {isChannel && <ChannelParticipants channelId={+id} />}
      </div>
      <Divider className={classes.divider} />
    </div>
  );
};

TitleMessage.defaultProps = {
  profil: '',
};

export default TitleMessage;

import { IconButton } from '@material-ui/core';
import { AddCircle } from '@material-ui/icons';
import { FC, useState } from 'react';
import Loading from '../../../../../../../common/Loading';
import ModalAddParticipants from '../../../../../../../common/ModalAddParticipants/ModalAddparticipants';
import { UserAvatarPopover } from '../../../../../../../common/UserAvatarPopover';
import {
  useGetAllUserQuery,
  useGetChannelMemberQuery,
  UserBasicInfosFragment,
} from '../../../../../../../generated/graphql';
import useStyle from './styles';

interface ChannelParticipantsProps {
  channelId: number;
}

const ChannelParticipants: FC<ChannelParticipantsProps> = props => {
  const { channelId } = props;
  const [isOpen, setIsOpen] = useState(false);
  const classes = useStyle();

  const { data: dataMemberUser, loading } = useGetChannelMemberQuery({
    variables: { channelId: +channelId },
  });

  const { data: userData, loading: userLoading } = useGetAllUserQuery();

  if (loading || userLoading) {
    return (
      <div className={classes.containerChannelParticipants}>
        <Loading size={25} />
      </div>
    );
  }

  const allUsers = (userData?.allUsers || []) as UserBasicInfosFragment[];
  const users = (
    dataMemberUser &&
    dataMemberUser.channelMembers &&
    dataMemberUser.channelMembers.members.length > 0
      ? dataMemberUser.channelMembers.members
      : []
  ) as UserBasicInfosFragment[];

  return (
    <div className={classes.containerChannelParticipants}>
      {users.map(user => (
        <UserAvatarPopover
          channelId={dataMemberUser?.channelMembers?.isOwner ? channelId : undefined}
          user={user}
          key={user.id}
        />
      ))}

      {dataMemberUser?.channelMembers?.isOwner && (
        <>
          <IconButton onClick={() => setIsOpen(true)}>
            <AddCircle className={classes.addParticipant} />
          </IconButton>
          <ModalAddParticipants
            channelId={channelId.toString()}
            memberUsers={users}
            users={allUsers}
            isOpen={isOpen}
            onClose={() => setIsOpen(false)}
          />
        </>
      )}
    </div>
  );
};

export default ChannelParticipants;

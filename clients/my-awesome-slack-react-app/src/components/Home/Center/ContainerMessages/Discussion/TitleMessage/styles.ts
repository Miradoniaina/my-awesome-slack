import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerTitleMessage: {},
  title: {
    marginLeft: 7,
  },
  titleContainer: {
    padding: '14px 14px',
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
  },
  divider: {
    flex: 1,
  },
}));

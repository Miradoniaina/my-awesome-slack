import { FC, KeyboardEvent, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { CustomInput } from '../../../../common/CustomInput';
import { globalSnackbarVar } from '../../../../config/reactiveVars';
import {
  useSendChannelMessageMutation,
  useSendPrivateMessageMutation,
} from '../../../../generated/graphql';
import { updateCacheAfterAddingChannelMessage } from '../../../../updateCache/channel/addChannelMessage';
import { updateCacheAfterAddingPrivateMessage } from '../../../../updateCache/channel/addPrivateMessage';
import { isChannelFn } from '../../../../utils/locationUtils';
import useStyle from './styles';

const MessageBox: FC = () => {
  const [message, setMessage] = useState('');
  const classes = useStyle();

  const { id } = useParams<{ id: string }>();

  const { pathname } = useLocation();
  const isChannel = isChannelFn(pathname);

  const [sendPrivateMessage, { loading: loadingSendPrivateMessage }] =
    useSendPrivateMessageMutation({
      update: updateCacheAfterAddingPrivateMessage,
    });

  const [sendChannelMessage, { loading: loadingSendChannel }] = useSendChannelMessageMutation({
    update: updateCacheAfterAddingChannelMessage,
  });

  const onSubmit = () => {
    if (message.trim().length) {
      if (!isChannel && !loadingSendPrivateMessage) {
        sendPrivateMessage({
          variables: {
            input: {
              message: message.trim(),
              receiverId: id,
            },
          },
        })
          .then(() => {
            setMessage('');
          })
          .catch(err => {
            globalSnackbarVar({
              open: true,
              message: err.message,
              type: 'ERROR',
            });
          });
      } else if (!loadingSendChannel) {
        sendChannelMessage({
          variables: {
            input: {
              message: message.trim(),
              channelId: id,
            },
          },
        })
          .then(() => {
            setMessage('');
          })
          .catch(err => {
            globalSnackbarVar({
              open: true,
              message: err.message,
              type: 'ERROR',
            });
          });
      }
    }
  };

  const onKeyboardClick = (evt: KeyboardEvent<HTMLInputElement>) => {
    if ((evt.ctrlKey || evt.shiftKey) && evt.key === 'Enter') {
      evt.preventDefault();
      onSubmit();
    }
  };

  return (
    <div className={classes.containerMessageBox}>
      <CustomInput
        className={classes.inputMessage}
        placeholder="Message..."
        value={message}
        setValue={setMessage}
        multiline={true}
        variant="filled"
        maxRows={4}
        onKeyPress={onKeyboardClick}
        onSubmit={onSubmit}
      />
    </div>
  );
};

export default MessageBox;

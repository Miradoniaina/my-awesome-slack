import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerMessageBox: {
    paddingLeft: 10,
    paddingRight: 15,
    marginBottom: 14,
    display: 'flex',
  },
  inputMessage: {
    flex: 1,
  },
}));

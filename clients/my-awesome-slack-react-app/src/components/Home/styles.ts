import { Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles<Theme>(theme => ({
  containerHome: {
    width: '100vw',
    height: '100vh',
    overflow: 'hidden',
    ...theme.containerStyles.flexColumn,
  },
  containerHomeBody: {
    ...theme.containerStyles.flexRow,
    flex: 1,
  },
}));

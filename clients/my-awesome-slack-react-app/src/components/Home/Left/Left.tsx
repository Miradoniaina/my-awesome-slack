import { FC } from 'react';
import { DirectMessagesSection } from '../DirectMessagesSection';
import { ChannelsSection } from './ChannelsSection';
import useStyle from './styles';
import { UserSection } from './UserSection';

const Left: FC = () => {
  const classes = useStyle();
  return (
    <div className={classes.containerLeft}>
      <UserSection />
      <div className={classes.centerSection}>
        <ChannelsSection />
        <DirectMessagesSection />
      </div>
    </div>
  );
};

export default Left;

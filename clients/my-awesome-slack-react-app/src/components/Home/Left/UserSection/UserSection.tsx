import { Typography } from '@material-ui/core';
import { FC } from 'react';
import { useMeQuery } from '../../../../generated/graphql';
import { getUserName } from '../../../../utils/user.utils';
import useStyle from './styles';

const UserSection: FC = () => {
  const classes = useStyle();
  const { data } = useMeQuery();
  const user = data?.me;
  if (!user) {
    return null;
  }
  const { userName } = user;
  return (
    <div className={classes.containerUserSection}>
      <Typography color="textSecondary" variant="h6">
        {userName}
        <Typography color="textSecondary" variant="caption">
          {` (@${getUserName(user)})`}
        </Typography>
      </Typography>
    </div>
  );
};

export default UserSection;

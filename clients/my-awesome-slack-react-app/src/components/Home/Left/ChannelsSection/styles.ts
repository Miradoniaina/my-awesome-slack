import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerChannelsSection: {
    marginTop: 45,
  },
  title: {
    paddingLeft: 14,
  },
}));

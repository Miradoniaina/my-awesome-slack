import { List, Typography } from '@material-ui/core';
import { FC, useEffect } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { ChannelMenuItem } from '../../../../common/ChannelMenuItem';
import { ErrorComponent } from '../../../../common/ErrorComponent';
import { Skeleton } from '../../../../common/Skeleton';
import {
  useMeQuery,
  useSubscribeChannelInvitationSubscription,
} from '../../../../generated/graphql';
import { updateCacheAfterSubscribsriptionChannelInvitation } from '../../../../updateCache/channel/subscriptionChannelInvitation';
import { isChannelFn } from '../../../../utils/locationUtils';
import { AddChannel } from './AddChannel';
import useStyle from './styles';

const ChannelsSection: FC = () => {
  const classes = useStyle();
  const { id } = useParams<{ id: string }>();
  const { pathname } = useLocation();

  const isChannel = isChannelFn(pathname);

  const { data, loading, error } = useMeQuery();
  useSubscribeChannelInvitationSubscription({
    onSubscriptionData: updateCacheAfterSubscribsriptionChannelInvitation,
  });

  const channels = data?.me?.channels || [];
  const firstChannelId = channels.length ? channels[0] && channels[0].id : null;

  const history = useHistory();

  useEffect(() => {
    if (!id && firstChannelId) {
      history.replace(`/channel/${firstChannelId}`);
    }
  }, [firstChannelId, id, history]);

  return (
    <div className={classes.containerChannelsSection}>
      <Typography className={classes.title} color="textSecondary">
        Channels
      </Typography>
      {error && <ErrorComponent error={error} />}
      {loading && (
        <>
          <Skeleton height={35} />
          <Skeleton height={35} />
          <Skeleton height={35} />
        </>
      )}
      {!loading && (
        <List>
          {channels.map(channel => (
            <ChannelMenuItem
              isActive={isChannel && (channel && channel.id) === id}
              name={channel && channel.name ? `${channel.name}` : ''}
              isPrivate={channel && channel.private ? channel.private : false}
              id={channel && channel.id ? `${channel.id}` : ''}
              key={channel && channel.id ? `${channel.id}` : ''}
            />
          ))}
        </List>
      )}
      <AddChannel />
    </div>
  );
};

export default ChannelsSection;

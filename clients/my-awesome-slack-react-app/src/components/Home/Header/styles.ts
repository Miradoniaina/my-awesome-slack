import { Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerHeader: {
    zIndex: theme.zIndex.appBar,
    backgroundColor: theme.colors.valentino,
    width: '100%',
    position: 'relative',
    boxShadow: theme.boxShadow.light,
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 4,
    paddingBottom: 4,
  },
  title: {
    marginLeft: 31,
    color: theme.colors.white,
  },
  avatar: {
    marginLeft: 'auto',
    marginRight: 7,
  },
}));

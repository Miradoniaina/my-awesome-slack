import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { FC, useEffect, useState } from 'react';
import { Avatar } from '../../../../common/Avatar';
import { useMeQuery } from '../../../../generated/graphql';
import { getUserName } from '../../../../utils/user.utils';

interface UserInfosProps {
  isOpen: boolean;
  close: () => void;
}

const UserInfos: FC<UserInfosProps> = props => {
  const { isOpen, close } = props;

  const { data } = useMeQuery();

  const user = data?.me;

  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [phone, setPhone] = useState('');

  useEffect(() => {
    if (user) {
      setFirstname(user.firstName);
      setLastname(user.lastName);
      if (user.phone) setPhone(user.phone);
    }
  }, [user]);

  if (!user) {
    return <p>An error occurred</p>;
  }

  return (
    <div>
      <Dialog open={isOpen} onClose={close} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Profil</DialogTitle>
        <DialogContent>
          <DialogContentText
            style={{
              display: 'flex',
              justifyContent: 'center',
            }}
          >
            <Avatar
              userName={getUserName(user)}
              size={250}
              indicatorStatus="isMine"
              profil={`${user.profil}`}
            />
          </DialogContentText>
          <TextField
            onChange={evt => setFirstname(evt.target.value)}
            margin="dense"
            value={firstname}
            label="Firstname"
            fullWidth={true}
            disabled={true}
            error={!firstname.trim()}
          />
          <TextField
            onChange={evt => setLastname(evt.target.value)}
            margin="dense"
            value={lastname}
            label="LastName"
            fullWidth={true}
            disabled={true}
            error={!lastname.trim()}
          />
          <TextField
            onChange={evt => setPhone(evt.target.value)}
            margin="dense"
            value={phone}
            label="Phone"
            fullWidth={true}
            disabled={true}
            error={!phone.trim()}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={close} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default UserInfos;

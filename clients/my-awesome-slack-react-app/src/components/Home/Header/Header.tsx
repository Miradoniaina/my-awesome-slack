import { Typography } from '@material-ui/core';
import { FC } from 'react';
import useStyle from './styles';
import UserAvatar from './UserAvatar';

interface HeaderProps {
  userFullName: string;
  profil: string;
}

const Header: FC<HeaderProps> = props => {
  const { userFullName, profil } = props;
  const classes = useStyle();

  return (
    <div className={classes.containerHeader}>
      <Typography variant="h5" className={classes.title}>
        My Awesome Slack
      </Typography>
      <UserAvatar userFullName={userFullName} profil={profil} />
    </div>
  );
};

export default Header;

/* eslint-disable no-unused-vars */
/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable no-use-before-define */
import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';

export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** Serializes and Deserializes Base64 strings */
  Base64: any;
  /** Date custom scalar type */
  DateTime: any;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
};

export type AuthPayload = {
  __typename?: 'AuthPayload';
  accessToken?: Maybe<Scalars['String']>;
};

export type Channel = {
  __typename?: 'Channel';
  id: Scalars['ID'];
  isOwner: Scalars['Boolean'];
  name: Scalars['String'];
  owner: User;
  private: Scalars['Boolean'];
};

export type ChannelMember = {
  __typename?: 'ChannelMember';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
};

export type ChannelMemberMutationPayload = {
  __typename?: 'ChannelMemberMutationPayload';
  id: Scalars['ID'];
  member: User;
  type?: Maybe<Scalars['String']>;
};

export type ChannelMemberPayload = {
  __typename?: 'ChannelMemberPayload';
  id: Scalars['ID'];
  isOwner: Scalars['Boolean'];
  members: Array<Maybe<User>>;
};

export type ChannelMessage = {
  __typename?: 'ChannelMessage';
  author: User;
  channelId: Scalars['String'];
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isMine: Scalars['Boolean'];
  message: Scalars['String'];
};

export type ChannelMessageQueryPayload = {
  __typename?: 'ChannelMessageQueryPayload';
  channelMessages: Array<Maybe<ChannelMessage>>;
  channelName: Scalars['String'];
  id: Scalars['ID'];
};

export type InputChannelMessages = {
  channelId: Scalars['Int'];
};

export type InputCreateChannelUser = {
  channel: Scalars['String'];
  private: Scalars['Boolean'];
};

export type InputLogin = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type InputPrivateMessage = {
  message: Scalars['String'];
  receiverId: Scalars['String'];
};

export type InputPrivateMessages = {
  privateUser: Scalars['Int'];
};

export type InputRemoveChannelParticipant = {
  channelId: Scalars['ID'];
  memberId: Scalars['ID'];
};

export type InputSendChannelMessage = {
  channelId: Scalars['String'];
  message: Scalars['String'];
};

export type InputSignUp = {
  email: Scalars['String'];
  firstName?: InputMaybe<Scalars['String']>;
  lastName?: InputMaybe<Scalars['String']>;
  password: Scalars['String'];
  phone: Scalars['String'];
  userName: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  addChannelParticipant?: Maybe<ChannelMemberMutationPayload>;
  createChannelUser?: Maybe<Channel>;
  login?: Maybe<AuthPayload>;
  removeChannelParticipant?: Maybe<ChannelMemberMutationPayload>;
  sendChannelMessage?: Maybe<ChannelMessage>;
  sendPrivateMessage?: Maybe<PrivateMessage>;
  signup?: Maybe<AuthPayload>;
};

export type MutationAddChannelParticipantArgs = {
  channel_id?: InputMaybe<Scalars['Int']>;
  member?: InputMaybe<Scalars['String']>;
};

export type MutationCreateChannelUserArgs = {
  input: InputCreateChannelUser;
};

export type MutationLoginArgs = {
  input: InputLogin;
};

export type MutationRemoveChannelParticipantArgs = {
  input: InputRemoveChannelParticipant;
};

export type MutationSendChannelMessageArgs = {
  input: InputSendChannelMessage;
};

export type MutationSendPrivateMessageArgs = {
  input: InputPrivateMessage;
};

export type MutationSignupArgs = {
  input: InputSignUp;
};

export type PrivateMessage = {
  __typename?: 'PrivateMessage';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isMine: Scalars['Boolean'];
  message: Scalars['String'];
  receiver: User;
  sender: User;
};

export type PrivateMessageItemPayload = {
  __typename?: 'PrivateMessageItemPayload';
  createdAt: Scalars['DateTime'];
  id: Scalars['ID'];
  isMine: Scalars['Boolean'];
  message: Scalars['String'];
  receiver: User;
  sender: User;
};

export type PrivateMessageQueryPayload = {
  __typename?: 'PrivateMessageQueryPayload';
  id: Scalars['ID'];
  privateMessages: Array<Maybe<PrivateMessage>>;
  privateUser: User;
};

export type Query = {
  __typename?: 'Query';
  allUsers?: Maybe<Array<Maybe<User>>>;
  channelMembers?: Maybe<ChannelMemberPayload>;
  channelMessages?: Maybe<ChannelMessageQueryPayload>;
  me?: Maybe<User>;
  privateMessages?: Maybe<PrivateMessageQueryPayload>;
};

export type QueryChannelMembersArgs = {
  channelId?: InputMaybe<Scalars['Int']>;
};

export type QueryChannelMessagesArgs = {
  input?: InputMaybe<InputChannelMessages>;
};

export type QueryMeArgs = {
  input?: InputMaybe<InputLogin>;
};

export type QueryPrivateMessagesArgs = {
  input?: InputMaybe<InputPrivateMessages>;
};

export type SendPrivateMessageMutationPayload = {
  __typename?: 'SendPrivateMessageMutationPayload';
  id: Scalars['ID'];
  privateMessage: PrivateMessage;
  privateUser: User;
};

export type Subscription = {
  __typename?: 'Subscription';
  addChannelMessage?: Maybe<ChannelMessage>;
  sendPrivateMessage?: Maybe<PrivateMessage>;
  userAddRemoveChannel?: Maybe<ChannelMemberMutationPayload>;
  userInviteChannel?: Maybe<Channel>;
  userLogsStatus?: Maybe<User>;
};

export type User = {
  __typename?: 'User';
  channels: Array<Channel>;
  email: Scalars['String'];
  firstName: Scalars['String'];
  id: Scalars['ID'];
  isConnected?: Maybe<Scalars['Boolean']>;
  isMe: Scalars['Boolean'];
  lastName: Scalars['String'];
  phone?: Maybe<Scalars['String']>;
  profil?: Maybe<Scalars['String']>;
  userName: Scalars['String'];
};

export type LoginMutationVariables = Exact<{
  input: InputLogin;
}>;

export type LoginMutation = {
  __typename?: 'Mutation';
  login?:
    | { __typename?: 'AuthPayload'; accessToken?: string | null | undefined }
    | null
    | undefined;
};

export type ChannelBasicInfosFragment = {
  __typename?: 'Channel';
  id: string;
  name: string;
  private: boolean;
  owner: {
    __typename?: 'User';
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    phone?: string | null | undefined;
    profil?: string | null | undefined;
    isMe: boolean;
    isConnected?: boolean | null | undefined;
  };
};

export type AddChannelParticipantMutationVariables = Exact<{
  channel_id?: InputMaybe<Scalars['Int']>;
  member?: InputMaybe<Scalars['String']>;
}>;

export type AddChannelParticipantMutation = {
  __typename?: 'Mutation';
  addChannelParticipant?:
    | {
        __typename?: 'ChannelMemberMutationPayload';
        id: string;
        member: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type RemoveChannelMemberMutationVariables = Exact<{
  input: InputRemoveChannelParticipant;
}>;

export type RemoveChannelMemberMutation = {
  __typename?: 'Mutation';
  removeChannelParticipant?:
    | {
        __typename?: 'ChannelMemberMutationPayload';
        id: string;
        member: { __typename?: 'User'; id: string };
      }
    | null
    | undefined;
};

export type GetChannelMemberQueryVariables = Exact<{
  channelId?: InputMaybe<Scalars['Int']>;
}>;

export type GetChannelMemberQuery = {
  __typename?: 'Query';
  channelMembers?:
    | {
        __typename?: 'ChannelMemberPayload';
        id: string;
        isOwner: boolean;
        members: Array<
          | {
              __typename?: 'User';
              id: string;
              firstName: string;
              lastName: string;
              userName: string;
              phone?: string | null | undefined;
              profil?: string | null | undefined;
              isMe: boolean;
              isConnected?: boolean | null | undefined;
            }
          | null
          | undefined
        >;
      }
    | null
    | undefined;
};

export type ChannelMessagesBasicInfosFragment = {
  __typename?: 'ChannelMessage';
  id: string;
  message: string;
  channelId: string;
  createdAt: any;
  isMine: boolean;
};

export type ChannelMessagesBasiFragementFragment = {
  __typename?: 'ChannelMessage';
  id: string;
  message: string;
  channelId: string;
  createdAt: any;
  isMine: boolean;
  author: {
    __typename?: 'User';
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    phone?: string | null | undefined;
    profil?: string | null | undefined;
    isMe: boolean;
    isConnected?: boolean | null | undefined;
  };
};

export type SendChannelMessageMutationVariables = Exact<{
  input: InputSendChannelMessage;
}>;

export type SendChannelMessageMutation = {
  __typename?: 'Mutation';
  sendChannelMessage?:
    | {
        __typename?: 'ChannelMessage';
        id: string;
        message: string;
        channelId: string;
        createdAt: any;
        isMine: boolean;
        author: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type GetChannelMessagesQueryVariables = Exact<{
  input?: InputMaybe<InputChannelMessages>;
}>;

export type GetChannelMessagesQuery = {
  __typename?: 'Query';
  channelMessages?:
    | {
        __typename?: 'ChannelMessageQueryPayload';
        id: string;
        channelName: string;
        channelMessages: Array<
          | {
              __typename?: 'ChannelMessage';
              id: string;
              message: string;
              channelId: string;
              createdAt: any;
              isMine: boolean;
              author: {
                __typename?: 'User';
                id: string;
                firstName: string;
                lastName: string;
                userName: string;
                phone?: string | null | undefined;
                profil?: string | null | undefined;
                isMe: boolean;
                isConnected?: boolean | null | undefined;
              };
            }
          | null
          | undefined
        >;
      }
    | null
    | undefined;
};

export type SubscribeToChannelMessageAddedSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToChannelMessageAddedSubscription = {
  __typename?: 'Subscription';
  addChannelMessage?:
    | {
        __typename?: 'ChannelMessage';
        id: string;
        message: string;
        channelId: string;
        createdAt: any;
        isMine: boolean;
        author: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type CreateChannelMutationVariables = Exact<{
  input: InputCreateChannelUser;
}>;

export type CreateChannelMutation = {
  __typename?: 'Mutation';
  createChannelUser?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        private: boolean;
        owner: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type SubscribeChannelInvitationSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeChannelInvitationSubscription = {
  __typename?: 'Subscription';
  userInviteChannel?:
    | {
        __typename?: 'Channel';
        id: string;
        name: string;
        private: boolean;
        owner: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type SubscribeToUserAddedRemovedInChannelSubscriptionVariables = Exact<{
  [key: string]: never;
}>;

export type SubscribeToUserAddedRemovedInChannelSubscription = {
  __typename?: 'Subscription';
  userAddRemoveChannel?:
    | {
        __typename?: 'ChannelMemberMutationPayload';
        id: string;
        type?: string | null | undefined;
        member: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type GetAllUserQueryVariables = Exact<{ [key: string]: never }>;

export type GetAllUserQuery = {
  __typename?: 'Query';
  allUsers?:
    | Array<
        | {
            __typename?: 'User';
            id: string;
            firstName: string;
            lastName: string;
            userName: string;
            phone?: string | null | undefined;
            profil?: string | null | undefined;
            isMe: boolean;
            isConnected?: boolean | null | undefined;
          }
        | null
        | undefined
      >
    | null
    | undefined;
};

export type PrivateMessageBasicInfosFragment = {
  __typename?: 'PrivateMessage';
  id: string;
  message: string;
  isMine: boolean;
  createdAt: any;
};

export type PrivateMessageItemInfosFragment = {
  __typename?: 'PrivateMessage';
  id: string;
  message: string;
  isMine: boolean;
  createdAt: any;
  sender: {
    __typename?: 'User';
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    phone?: string | null | undefined;
    profil?: string | null | undefined;
    isMe: boolean;
    isConnected?: boolean | null | undefined;
  };
  receiver: {
    __typename?: 'User';
    id: string;
    firstName: string;
    lastName: string;
    userName: string;
    phone?: string | null | undefined;
    profil?: string | null | undefined;
    isMe: boolean;
    isConnected?: boolean | null | undefined;
  };
};

export type SendPrivateMessageMutationVariables = Exact<{
  input: InputPrivateMessage;
}>;

export type SendPrivateMessageMutation = {
  __typename?: 'Mutation';
  sendPrivateMessage?:
    | {
        __typename?: 'PrivateMessage';
        id: string;
        message: string;
        isMine: boolean;
        createdAt: any;
        sender: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
        receiver: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type GetPrivateMessageQueryVariables = Exact<{
  input?: InputMaybe<InputPrivateMessages>;
}>;

export type GetPrivateMessageQuery = {
  __typename?: 'Query';
  privateMessages?:
    | {
        __typename?: 'PrivateMessageQueryPayload';
        id: string;
        privateUser: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
        privateMessages: Array<
          | {
              __typename?: 'PrivateMessage';
              id: string;
              message: string;
              isMine: boolean;
              createdAt: any;
              sender: {
                __typename?: 'User';
                id: string;
                firstName: string;
                lastName: string;
                userName: string;
                phone?: string | null | undefined;
                profil?: string | null | undefined;
                isMe: boolean;
                isConnected?: boolean | null | undefined;
              };
              receiver: {
                __typename?: 'User';
                id: string;
                firstName: string;
                lastName: string;
                userName: string;
                phone?: string | null | undefined;
                profil?: string | null | undefined;
                isMe: boolean;
                isConnected?: boolean | null | undefined;
              };
            }
          | null
          | undefined
        >;
      }
    | null
    | undefined;
};

export type SubscribeToPrivateMessageAddedSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToPrivateMessageAddedSubscription = {
  __typename?: 'Subscription';
  sendPrivateMessage?:
    | {
        __typename?: 'PrivateMessage';
        id: string;
        message: string;
        isMine: boolean;
        createdAt: any;
        sender: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
        receiver: {
          __typename?: 'User';
          id: string;
          firstName: string;
          lastName: string;
          userName: string;
          phone?: string | null | undefined;
          profil?: string | null | undefined;
          isMe: boolean;
          isConnected?: boolean | null | undefined;
        };
      }
    | null
    | undefined;
};

export type UserBasicInfosFragment = {
  __typename?: 'User';
  id: string;
  firstName: string;
  lastName: string;
  userName: string;
  phone?: string | null | undefined;
  profil?: string | null | undefined;
  isMe: boolean;
  isConnected?: boolean | null | undefined;
};

export type MeQueryVariables = Exact<{ [key: string]: never }>;

export type MeQuery = {
  __typename?: 'Query';
  me?:
    | {
        __typename?: 'User';
        id: string;
        firstName: string;
        lastName: string;
        userName: string;
        phone?: string | null | undefined;
        profil?: string | null | undefined;
        isMe: boolean;
        isConnected?: boolean | null | undefined;
        channels: Array<{
          __typename?: 'Channel';
          id: string;
          name: string;
          private: boolean;
          owner: {
            __typename?: 'User';
            id: string;
            firstName: string;
            lastName: string;
            userName: string;
            phone?: string | null | undefined;
            profil?: string | null | undefined;
            isMe: boolean;
            isConnected?: boolean | null | undefined;
          };
        }>;
      }
    | null
    | undefined;
};

export type SubscribeToUserLogsStatusSubscriptionVariables = Exact<{ [key: string]: never }>;

export type SubscribeToUserLogsStatusSubscription = {
  __typename?: 'Subscription';
  userLogsStatus?:
    | { __typename?: 'User'; id: string; isConnected?: boolean | null | undefined }
    | null
    | undefined;
};

export const UserBasicInfosFragmentDoc = gql`
  fragment UserBasicInfos on User {
    id
    firstName
    lastName
    userName
    phone
    profil
    isMe
    isConnected
  }
`;
export const ChannelBasicInfosFragmentDoc = gql`
  fragment ChannelBasicInfos on Channel {
    id
    name
    private
    owner {
      ...UserBasicInfos
    }
    private
  }
  ${UserBasicInfosFragmentDoc}
`;
export const ChannelMessagesBasicInfosFragmentDoc = gql`
  fragment ChannelMessagesBasicInfos on ChannelMessage {
    id
    message
    channelId
    createdAt
    isMine
  }
`;
export const ChannelMessagesBasiFragementFragmentDoc = gql`
  fragment ChannelMessagesBasiFragement on ChannelMessage {
    ...ChannelMessagesBasicInfos
    author {
      ...UserBasicInfos
    }
  }
  ${ChannelMessagesBasicInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;
export const PrivateMessageBasicInfosFragmentDoc = gql`
  fragment PrivateMessageBasicInfos on PrivateMessage {
    id
    message
    isMine
    createdAt
  }
`;
export const PrivateMessageItemInfosFragmentDoc = gql`
  fragment PrivateMessageItemInfos on PrivateMessage {
    ...PrivateMessageBasicInfos
    sender {
      ...UserBasicInfos
    }
    receiver {
      ...UserBasicInfos
    }
  }
  ${PrivateMessageBasicInfosFragmentDoc}
  ${UserBasicInfosFragmentDoc}
`;
export const LoginDocument = gql`
  mutation Login($input: InputLogin!) {
    login(input: $input) {
      accessToken
    }
  }
`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useLoginMutation(
  baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<
  LoginMutation,
  LoginMutationVariables
>;
export const AddChannelParticipantDocument = gql`
  mutation AddChannelParticipant($channel_id: Int, $member: String) {
    addChannelParticipant(channel_id: $channel_id, member: $member) {
      id
      member {
        ...UserBasicInfos
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
`;
export type AddChannelParticipantMutationFn = Apollo.MutationFunction<
  AddChannelParticipantMutation,
  AddChannelParticipantMutationVariables
>;

/**
 * __useAddChannelParticipantMutation__
 *
 * To run a mutation, you first call `useAddChannelParticipantMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAddChannelParticipantMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [addChannelParticipantMutation, { data, loading, error }] = useAddChannelParticipantMutation({
 *   variables: {
 *      channel_id: // value for 'channel_id'
 *      member: // value for 'member'
 *   },
 * });
 */
export function useAddChannelParticipantMutation(
  baseOptions?: Apollo.MutationHookOptions<
    AddChannelParticipantMutation,
    AddChannelParticipantMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<AddChannelParticipantMutation, AddChannelParticipantMutationVariables>(
    AddChannelParticipantDocument,
    options
  );
}
export type AddChannelParticipantMutationHookResult = ReturnType<
  typeof useAddChannelParticipantMutation
>;
export type AddChannelParticipantMutationResult =
  Apollo.MutationResult<AddChannelParticipantMutation>;
export type AddChannelParticipantMutationOptions = Apollo.BaseMutationOptions<
  AddChannelParticipantMutation,
  AddChannelParticipantMutationVariables
>;
export const RemoveChannelMemberDocument = gql`
  mutation RemoveChannelMember($input: InputRemoveChannelParticipant!) {
    removeChannelParticipant(input: $input) {
      id
      member {
        id
      }
    }
  }
`;
export type RemoveChannelMemberMutationFn = Apollo.MutationFunction<
  RemoveChannelMemberMutation,
  RemoveChannelMemberMutationVariables
>;

/**
 * __useRemoveChannelMemberMutation__
 *
 * To run a mutation, you first call `useRemoveChannelMemberMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveChannelMemberMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeChannelMemberMutation, { data, loading, error }] = useRemoveChannelMemberMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useRemoveChannelMemberMutation(
  baseOptions?: Apollo.MutationHookOptions<
    RemoveChannelMemberMutation,
    RemoveChannelMemberMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<RemoveChannelMemberMutation, RemoveChannelMemberMutationVariables>(
    RemoveChannelMemberDocument,
    options
  );
}
export type RemoveChannelMemberMutationHookResult = ReturnType<
  typeof useRemoveChannelMemberMutation
>;
export type RemoveChannelMemberMutationResult = Apollo.MutationResult<RemoveChannelMemberMutation>;
export type RemoveChannelMemberMutationOptions = Apollo.BaseMutationOptions<
  RemoveChannelMemberMutation,
  RemoveChannelMemberMutationVariables
>;
export const GetChannelMemberDocument = gql`
  query GetChannelMember($channelId: Int) {
    channelMembers(channelId: $channelId) {
      id
      isOwner
      members {
        ...UserBasicInfos
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetChannelMemberQuery__
 *
 * To run a query within a React component, call `useGetChannelMemberQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChannelMemberQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChannelMemberQuery({
 *   variables: {
 *      channelId: // value for 'channelId'
 *   },
 * });
 */
export function useGetChannelMemberQuery(
  baseOptions?: Apollo.QueryHookOptions<GetChannelMemberQuery, GetChannelMemberQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetChannelMemberQuery, GetChannelMemberQueryVariables>(
    GetChannelMemberDocument,
    options
  );
}
export function useGetChannelMemberLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetChannelMemberQuery, GetChannelMemberQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetChannelMemberQuery, GetChannelMemberQueryVariables>(
    GetChannelMemberDocument,
    options
  );
}
export type GetChannelMemberQueryHookResult = ReturnType<typeof useGetChannelMemberQuery>;
export type GetChannelMemberLazyQueryHookResult = ReturnType<typeof useGetChannelMemberLazyQuery>;
export type GetChannelMemberQueryResult = Apollo.QueryResult<
  GetChannelMemberQuery,
  GetChannelMemberQueryVariables
>;
export const SendChannelMessageDocument = gql`
  mutation SendChannelMessage($input: InputSendChannelMessage!) {
    sendChannelMessage(input: $input) {
      ...ChannelMessagesBasiFragement
    }
  }
  ${ChannelMessagesBasiFragementFragmentDoc}
`;
export type SendChannelMessageMutationFn = Apollo.MutationFunction<
  SendChannelMessageMutation,
  SendChannelMessageMutationVariables
>;

/**
 * __useSendChannelMessageMutation__
 *
 * To run a mutation, you first call `useSendChannelMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendChannelMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendChannelMessageMutation, { data, loading, error }] = useSendChannelMessageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSendChannelMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<
    SendChannelMessageMutation,
    SendChannelMessageMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<SendChannelMessageMutation, SendChannelMessageMutationVariables>(
    SendChannelMessageDocument,
    options
  );
}
export type SendChannelMessageMutationHookResult = ReturnType<typeof useSendChannelMessageMutation>;
export type SendChannelMessageMutationResult = Apollo.MutationResult<SendChannelMessageMutation>;
export type SendChannelMessageMutationOptions = Apollo.BaseMutationOptions<
  SendChannelMessageMutation,
  SendChannelMessageMutationVariables
>;
export const GetChannelMessagesDocument = gql`
  query GetChannelMessages($input: InputChannelMessages) {
    channelMessages(input: $input) {
      id
      channelName
      channelMessages {
        ...ChannelMessagesBasiFragement
      }
    }
  }
  ${ChannelMessagesBasiFragementFragmentDoc}
`;

/**
 * __useGetChannelMessagesQuery__
 *
 * To run a query within a React component, call `useGetChannelMessagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetChannelMessagesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetChannelMessagesQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetChannelMessagesQuery(
  baseOptions?: Apollo.QueryHookOptions<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>(
    GetChannelMessagesDocument,
    options
  );
}
export function useGetChannelMessagesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetChannelMessagesQuery,
    GetChannelMessagesQueryVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetChannelMessagesQuery, GetChannelMessagesQueryVariables>(
    GetChannelMessagesDocument,
    options
  );
}
export type GetChannelMessagesQueryHookResult = ReturnType<typeof useGetChannelMessagesQuery>;
export type GetChannelMessagesLazyQueryHookResult = ReturnType<
  typeof useGetChannelMessagesLazyQuery
>;
export type GetChannelMessagesQueryResult = Apollo.QueryResult<
  GetChannelMessagesQuery,
  GetChannelMessagesQueryVariables
>;
export const SubscribeToChannelMessageAddedDocument = gql`
  subscription SubscribeToChannelMessageAdded {
    addChannelMessage {
      ...ChannelMessagesBasiFragement
    }
  }
  ${ChannelMessagesBasiFragementFragmentDoc}
`;

/**
 * __useSubscribeToChannelMessageAddedSubscription__
 *
 * To run a query within a React component, call `useSubscribeToChannelMessageAddedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToChannelMessageAddedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToChannelMessageAddedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToChannelMessageAddedSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToChannelMessageAddedSubscription,
    SubscribeToChannelMessageAddedSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToChannelMessageAddedSubscription,
    SubscribeToChannelMessageAddedSubscriptionVariables
  >(SubscribeToChannelMessageAddedDocument, options);
}
export type SubscribeToChannelMessageAddedSubscriptionHookResult = ReturnType<
  typeof useSubscribeToChannelMessageAddedSubscription
>;
export type SubscribeToChannelMessageAddedSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToChannelMessageAddedSubscription>;
export const CreateChannelDocument = gql`
  mutation CreateChannel($input: InputCreateChannelUser!) {
    createChannelUser(input: $input) {
      ...ChannelBasicInfos
    }
  }
  ${ChannelBasicInfosFragmentDoc}
`;
export type CreateChannelMutationFn = Apollo.MutationFunction<
  CreateChannelMutation,
  CreateChannelMutationVariables
>;

/**
 * __useCreateChannelMutation__
 *
 * To run a mutation, you first call `useCreateChannelMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateChannelMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createChannelMutation, { data, loading, error }] = useCreateChannelMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateChannelMutation(
  baseOptions?: Apollo.MutationHookOptions<CreateChannelMutation, CreateChannelMutationVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<CreateChannelMutation, CreateChannelMutationVariables>(
    CreateChannelDocument,
    options
  );
}
export type CreateChannelMutationHookResult = ReturnType<typeof useCreateChannelMutation>;
export type CreateChannelMutationResult = Apollo.MutationResult<CreateChannelMutation>;
export type CreateChannelMutationOptions = Apollo.BaseMutationOptions<
  CreateChannelMutation,
  CreateChannelMutationVariables
>;
export const SubscribeChannelInvitationDocument = gql`
  subscription SubscribeChannelInvitation {
    userInviteChannel {
      ...ChannelBasicInfos
    }
  }
  ${ChannelBasicInfosFragmentDoc}
`;

/**
 * __useSubscribeChannelInvitationSubscription__
 *
 * To run a query within a React component, call `useSubscribeChannelInvitationSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeChannelInvitationSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeChannelInvitationSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeChannelInvitationSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeChannelInvitationSubscription,
    SubscribeChannelInvitationSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeChannelInvitationSubscription,
    SubscribeChannelInvitationSubscriptionVariables
  >(SubscribeChannelInvitationDocument, options);
}
export type SubscribeChannelInvitationSubscriptionHookResult = ReturnType<
  typeof useSubscribeChannelInvitationSubscription
>;
export type SubscribeChannelInvitationSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeChannelInvitationSubscription>;
export const SubscribeToUserAddedRemovedInChannelDocument = gql`
  subscription SubscribeToUserAddedRemovedInChannel {
    userAddRemoveChannel {
      id
      member {
        ...UserBasicInfos
      }
      type
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useSubscribeToUserAddedRemovedInChannelSubscription__
 *
 * To run a query within a React component, call `useSubscribeToUserAddedRemovedInChannelSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToUserAddedRemovedInChannelSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToUserAddedRemovedInChannelSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToUserAddedRemovedInChannelSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToUserAddedRemovedInChannelSubscription,
    SubscribeToUserAddedRemovedInChannelSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToUserAddedRemovedInChannelSubscription,
    SubscribeToUserAddedRemovedInChannelSubscriptionVariables
  >(SubscribeToUserAddedRemovedInChannelDocument, options);
}
export type SubscribeToUserAddedRemovedInChannelSubscriptionHookResult = ReturnType<
  typeof useSubscribeToUserAddedRemovedInChannelSubscription
>;
export type SubscribeToUserAddedRemovedInChannelSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToUserAddedRemovedInChannelSubscription>;
export const GetAllUserDocument = gql`
  query GetAllUser {
    allUsers {
      ...UserBasicInfos
    }
  }
  ${UserBasicInfosFragmentDoc}
`;

/**
 * __useGetAllUserQuery__
 *
 * To run a query within a React component, call `useGetAllUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllUserQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllUserQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllUserQuery(
  baseOptions?: Apollo.QueryHookOptions<GetAllUserQuery, GetAllUserQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetAllUserQuery, GetAllUserQueryVariables>(GetAllUserDocument, options);
}
export function useGetAllUserLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetAllUserQuery, GetAllUserQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetAllUserQuery, GetAllUserQueryVariables>(
    GetAllUserDocument,
    options
  );
}
export type GetAllUserQueryHookResult = ReturnType<typeof useGetAllUserQuery>;
export type GetAllUserLazyQueryHookResult = ReturnType<typeof useGetAllUserLazyQuery>;
export type GetAllUserQueryResult = Apollo.QueryResult<GetAllUserQuery, GetAllUserQueryVariables>;
export const SendPrivateMessageDocument = gql`
  mutation SendPrivateMessage($input: InputPrivateMessage!) {
    sendPrivateMessage(input: $input) {
      ...PrivateMessageItemInfos
    }
  }
  ${PrivateMessageItemInfosFragmentDoc}
`;
export type SendPrivateMessageMutationFn = Apollo.MutationFunction<
  SendPrivateMessageMutation,
  SendPrivateMessageMutationVariables
>;

/**
 * __useSendPrivateMessageMutation__
 *
 * To run a mutation, you first call `useSendPrivateMessageMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSendPrivateMessageMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [sendPrivateMessageMutation, { data, loading, error }] = useSendPrivateMessageMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSendPrivateMessageMutation(
  baseOptions?: Apollo.MutationHookOptions<
    SendPrivateMessageMutation,
    SendPrivateMessageMutationVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useMutation<SendPrivateMessageMutation, SendPrivateMessageMutationVariables>(
    SendPrivateMessageDocument,
    options
  );
}
export type SendPrivateMessageMutationHookResult = ReturnType<typeof useSendPrivateMessageMutation>;
export type SendPrivateMessageMutationResult = Apollo.MutationResult<SendPrivateMessageMutation>;
export type SendPrivateMessageMutationOptions = Apollo.BaseMutationOptions<
  SendPrivateMessageMutation,
  SendPrivateMessageMutationVariables
>;
export const GetPrivateMessageDocument = gql`
  query GetPrivateMessage($input: InputPrivateMessages) {
    privateMessages(input: $input) {
      id
      privateUser {
        ...UserBasicInfos
      }
      privateMessages {
        ...PrivateMessageItemInfos
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
  ${PrivateMessageItemInfosFragmentDoc}
`;

/**
 * __useGetPrivateMessageQuery__
 *
 * To run a query within a React component, call `useGetPrivateMessageQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPrivateMessageQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPrivateMessageQuery({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useGetPrivateMessageQuery(
  baseOptions?: Apollo.QueryHookOptions<GetPrivateMessageQuery, GetPrivateMessageQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<GetPrivateMessageQuery, GetPrivateMessageQueryVariables>(
    GetPrivateMessageDocument,
    options
  );
}
export function useGetPrivateMessageLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<GetPrivateMessageQuery, GetPrivateMessageQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<GetPrivateMessageQuery, GetPrivateMessageQueryVariables>(
    GetPrivateMessageDocument,
    options
  );
}
export type GetPrivateMessageQueryHookResult = ReturnType<typeof useGetPrivateMessageQuery>;
export type GetPrivateMessageLazyQueryHookResult = ReturnType<typeof useGetPrivateMessageLazyQuery>;
export type GetPrivateMessageQueryResult = Apollo.QueryResult<
  GetPrivateMessageQuery,
  GetPrivateMessageQueryVariables
>;
export const SubscribeToPrivateMessageAddedDocument = gql`
  subscription SubscribeToPrivateMessageAdded {
    sendPrivateMessage {
      ...PrivateMessageItemInfos
    }
  }
  ${PrivateMessageItemInfosFragmentDoc}
`;

/**
 * __useSubscribeToPrivateMessageAddedSubscription__
 *
 * To run a query within a React component, call `useSubscribeToPrivateMessageAddedSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToPrivateMessageAddedSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToPrivateMessageAddedSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToPrivateMessageAddedSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToPrivateMessageAddedSubscription,
    SubscribeToPrivateMessageAddedSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToPrivateMessageAddedSubscription,
    SubscribeToPrivateMessageAddedSubscriptionVariables
  >(SubscribeToPrivateMessageAddedDocument, options);
}
export type SubscribeToPrivateMessageAddedSubscriptionHookResult = ReturnType<
  typeof useSubscribeToPrivateMessageAddedSubscription
>;
export type SubscribeToPrivateMessageAddedSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToPrivateMessageAddedSubscription>;
export const MeDocument = gql`
  query me {
    me {
      ...UserBasicInfos
      channels {
        ...ChannelBasicInfos
      }
    }
  }
  ${UserBasicInfosFragmentDoc}
  ${ChannelBasicInfosFragmentDoc}
`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export function useMeLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
}
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;
export const SubscribeToUserLogsStatusDocument = gql`
  subscription SubscribeToUserLogsStatus {
    userLogsStatus {
      id
      isConnected
    }
  }
`;

/**
 * __useSubscribeToUserLogsStatusSubscription__
 *
 * To run a query within a React component, call `useSubscribeToUserLogsStatusSubscription` and pass it any options that fit your needs.
 * When your component renders, `useSubscribeToUserLogsStatusSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSubscribeToUserLogsStatusSubscription({
 *   variables: {
 *   },
 * });
 */
export function useSubscribeToUserLogsStatusSubscription(
  baseOptions?: Apollo.SubscriptionHookOptions<
    SubscribeToUserLogsStatusSubscription,
    SubscribeToUserLogsStatusSubscriptionVariables
  >
) {
  const options = { ...defaultOptions, ...baseOptions };
  return Apollo.useSubscription<
    SubscribeToUserLogsStatusSubscription,
    SubscribeToUserLogsStatusSubscriptionVariables
  >(SubscribeToUserLogsStatusDocument, options);
}
export type SubscribeToUserLogsStatusSubscriptionHookResult = ReturnType<
  typeof useSubscribeToUserLogsStatusSubscription
>;
export type SubscribeToUserLogsStatusSubscriptionResult =
  Apollo.SubscriptionResult<SubscribeToUserLogsStatusSubscription>;

export interface PossibleTypesResultData {
  possibleTypes: {
    [key: string]: string[];
  };
}
const result: PossibleTypesResultData = {
  possibleTypes: {},
};
export default result;

import { lazy } from 'react';
import { Switch } from 'react-router-dom';
import ProtectedLazyRoute from '../common/ProtectedLazyRoute';

const Authentication = lazy(() => import('../components/Authentication'));
const Home = lazy(() => import('../components/Home/Home'));
const PageNotFound = lazy(() => import('../components/PageNotFound'));

const Router = () => {
  return (
    <Switch>
      <ProtectedLazyRoute
        path="/login"
        exact={true}
        access="authentication"
        component={Authentication}
      />
      <ProtectedLazyRoute
        exact={true}
        path={['/channel/:id(\\d+)', '/message/:id(\\d+)', '/']}
        component={Home}
      />
      <ProtectedLazyRoute path="*" exact={true} component={PageNotFound} access="public" />
    </Switch>
  );
};

export default Router;

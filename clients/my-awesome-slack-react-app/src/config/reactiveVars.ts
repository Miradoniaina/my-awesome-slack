import { makeVar } from '@apollo/client';
import { SnackbarOrigin } from '@material-ui/core/Snackbar';

export const globalSnackbarVar = makeVar<{
  open: boolean;
  duration?: number;
  message: string;
  type: 'ERROR' | 'SUCCESS' | 'DEFAULT';
  anchor?: SnackbarOrigin;
}>({
  open: false,
  duration: 2000,
  message: '',
  type: 'DEFAULT',
  anchor: {
    horizontal: 'right',
    vertical: 'top',
  },
});

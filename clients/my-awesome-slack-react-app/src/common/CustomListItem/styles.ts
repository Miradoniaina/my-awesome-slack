import makeStyles from '@material-ui/core/styles/makeStyles';
import { important } from '../../utils/materialUtils';

export default makeStyles(theme => ({
  containerCustomListItem: {},
  selected: {
    backgroundColor: important(theme.colors.brilliantRose),
  },
  textSelected: {
    color: theme.colors.black,
  },
}));

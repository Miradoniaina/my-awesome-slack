import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerRemoveUserFromChannel: {
    color: 'red',
    marginTop: 21,
  },
}));

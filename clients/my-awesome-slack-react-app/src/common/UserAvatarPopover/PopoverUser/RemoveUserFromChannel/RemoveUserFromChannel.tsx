import { Button } from '@material-ui/core';
import { FC } from 'react';
import { useRemoveChannelMemberMutation } from '../../../../generated/graphql';
import { updateCacheAfterRemovingParticipant } from '../../../../updateCache/channel/removedChannel';
import useStyle from './styles';

interface RemoveUserFromChannelProps {
  userId: number;
  chanelId: number;
  onClose: () => void;
}

const RemoveUserFromChannel: FC<RemoveUserFromChannelProps> = props => {
  const { userId, chanelId, onClose } = props;
  const [mutationRemoveUser, { loading }] = useRemoveChannelMemberMutation({
    update: updateCacheAfterRemovingParticipant,
  });

  const classes = useStyle();

  const onClick = () => {
    if (!loading) {
      mutationRemoveUser({
        variables: {
          input: {
            channelId: chanelId.toString(),
            memberId: userId.toString(),
          },
        },
        optimisticResponse: {
          __typename: 'Mutation',
          removeChannelParticipant: {
            id: chanelId.toString(),
            member: {
              id: userId.toString(),
            },
          },
        },
      }).then(() => onClose());
    }
  };

  return (
    <Button onClick={onClick} className={classes.containerRemoveUserFromChannel} fullWidth={true}>
      Remove
    </Button>
  );
};

export default RemoveUserFromChannel;

import { Paper, Popover } from '@material-ui/core';
import { FC } from 'react';
import { UserBasicInfosFragment } from '../../../generated/graphql';
import { getUserName } from '../../../utils/user.utils';
import { Avatar } from '../../Avatar';
import { RemoveUserFromChannel } from './RemoveUserFromChannel';
import useStyle from './styles';

interface PopoverUserProps {
  anchorEl: HTMLDivElement | null;
  isOpen: boolean;
  onClose: () => void;
  user: UserBasicInfosFragment;
  chanelId?: number;
}

const PopoverUser: FC<PopoverUserProps> = props => {
  const { anchorEl, onClose, isOpen, user, chanelId } = props;
  const { firstName, lastName, profil, phone, isConnected, isMe } = user;
  const classes = useStyle();
  const name = getUserName(user);

  return (
    <Popover
      open={isOpen}
      anchorEl={anchorEl}
      onClose={onClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
    >
      <Paper className={classes.containerPopoverUser}>
        <Avatar
          indicatorStatus={isConnected ? 'online' : 'offline'}
          size={54}
          userName={name}
          profil={profil || ''}
          isCircle={true}
        />

        <div className={classes.containerInfos}>
          <p>
            <span className={classes.title}>nom:</span>
            <span>{firstName}</span>
          </p>
          <p>
            <span className={classes.title}>prénom:</span>
            <span>{lastName}</span>
          </p>
          {phone && (
            <p>
              <span className={classes.title}>Phone:</span>
              <span>{phone}</span>
            </p>
          )}
        </div>
        {chanelId && !isMe && (
          <RemoveUserFromChannel onClose={onClose} chanelId={chanelId} userId={+user.id} />
        )}
      </Paper>
    </Popover>
  );
};

PopoverUser.defaultProps = {
  chanelId: undefined,
};

export default PopoverUser;

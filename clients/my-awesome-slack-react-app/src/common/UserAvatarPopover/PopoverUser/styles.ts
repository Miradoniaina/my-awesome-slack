import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerPopoverUser: {
    minWidth: 200,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 14,
  },
  containerInfos: {
    marginTop: 21,
  },
  title: {
    fontWeight: 500,
    marginRight: 4,
  },
}));

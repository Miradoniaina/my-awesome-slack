import { FC, useCallback, useRef, useState } from 'react';
import { UserBasicInfosFragment } from '../../generated/graphql';
import { getUserName } from '../../utils/user.utils';
import { Avatar } from '../Avatar';
import { PopoverUser } from './PopoverUser';

interface UserAvatarPopoverProps {
  user: UserBasicInfosFragment;
  channelId?: number;
}

const UserAvatarPopover: FC<UserAvatarPopoverProps> = props => {
  const { user, channelId } = props;

  const { isConnected, profil } = user;
  const name = getUserName(user);

  const avatarRef = useRef<HTMLDivElement>(null);

  const [isPopoverOpened, setIsPopoverOpened] = useState(false);

  const onClose = useCallback(() => {
    setIsPopoverOpened(false);
  }, []);

  const openPopover = useCallback(() => {
    setIsPopoverOpened(true);
  }, []);

  return (
    <>
      <Avatar
        indicatorStatus={isConnected ? 'online' : 'offline'}
        size={54}
        userName={name}
        profil={profil || ''}
        isCircle={true}
        ref={avatarRef}
        onClick={openPopover}
      />
      <PopoverUser
        user={user}
        anchorEl={avatarRef.current}
        isOpen={isPopoverOpened}
        onClose={onClose}
        chanelId={channelId}
      />
    </>
  );
};

UserAvatarPopover.defaultProps = {
  channelId: undefined,
};

export default UserAvatarPopover;

import { Avatar as MaterialAvatar, Tooltip } from '@material-ui/core';
import classnames from 'classnames';
import { forwardRef } from 'react';
import { OnlineIndicator } from '../OnlineIndicator';
import useStyle from './styles';

interface AvatarProps {
  userName: string;
  profil: string;
  indicatorStatus?: 'isMine' | 'offline' | 'online';
  size?: number;
  isCircle?: boolean;
  className?: string;
  onClick?: () => void;
}

const Avatar = forwardRef<HTMLDivElement, AvatarProps>((props, ref) => {
  const { isCircle, userName, indicatorStatus, className, size, profil, onClick, ...otherProps } =
    props;

  const classes = useStyle();

  return (
    <div
      style={{
        maxHeight: size,
        maxWidth: size,
      }}
      {...otherProps}
      className={classnames(classes.containerAvatar, className, onClick && classes.clickable)}
      ref={ref}
      onClick={onClick}
    >
      <Tooltip title={userName} enterDelay={200}>
        <MaterialAvatar
          style={{
            width: size,
            height: size,
          }}
          title={userName}
          src={profil}
          variant={!isCircle ? 'rounded' : 'circle'}
        >
          {userName.substr(0, 1).toUpperCase()}
        </MaterialAvatar>
      </Tooltip>
      {indicatorStatus && (
        <OnlineIndicator status={indicatorStatus} className={classes.onlineIndicator} />
      )}
    </div>
  );
});

Avatar.defaultProps = {
  indicatorStatus: undefined,
  size: undefined,
  isCircle: false,
  className: '',
  onClick: undefined,
};

export default Avatar;

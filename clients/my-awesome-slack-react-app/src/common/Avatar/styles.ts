import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerAvatar: {
    position: 'relative',
    display: 'inline-block',
  },
  onlineIndicator: {
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  clickable: {
    cursor: 'pointer',
  },
}));

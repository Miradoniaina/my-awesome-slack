import classnames from 'classnames';
import { FC } from 'react';
import useStyle from './styles';

interface OfflineProps {
  size: number;
  isOffline: boolean;
}

const Offline: FC<OfflineProps> = props => {
  const { size, isOffline } = props;
  const classes = useStyle();
  return (
    <div
      className={classnames(
        classes.containerIndicator,
        !isOffline && classes.containerIndicatorOnline
      )}
      style={{
        width: size,
        height: size,
      }}
    />
  );
};

export default Offline;

import { Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles((theme: Theme) => ({
  containerIndicator: {
    borderRadius: '50%',
    border: `${theme.colors.white} 2px solid`,
    backgroundColor: theme.colors.black,
  },
  containerIndicatorOnline: {
    border: `${theme.colors.black} 2px solid`,
    backgroundColor: theme.colors.jungleGreen,
  },
}));

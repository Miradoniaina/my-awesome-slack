import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(() => ({
  containerMessageItemText: {
    whiteSpace: 'pre-line',
  },
}));

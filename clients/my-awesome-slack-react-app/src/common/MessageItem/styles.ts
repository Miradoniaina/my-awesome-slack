import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerMessageItem: {
    ...theme.containerStyles.flexRow,
    display: 'flex',
  },
  mine: {
    justifyContent: 'flex-end',
  },
  messageContainer: {
    marginLeft: 7,
    ...theme.containerStyles.flexColumn,
  },
  authorContainer: {
    ...theme.containerStyles.flexColumn,
    alignItems: 'flex-start',
  },
  time: {
    marginLeft: 21,
    fontStyle: 'italic',
    marginTop: 2,
    alignSelf: 'flex-end',
  },
}));

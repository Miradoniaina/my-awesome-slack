import classnames from 'classnames';
import { FC } from 'react';
import {
  ChannelMessagesBasiFragementFragment,
  PrivateMessageItemInfosFragment,
} from '../../generated/graphql';
import { getUserName } from '../../utils/user.utils';
import { Avatar } from '../Avatar';
import { TimeAgo } from '../TimeAgo';
import MessageItemSender from './MessageItemSender/MessageItemSender';
import { MessageItemText } from './MessageItemText';
import useStyle from './styles';

interface MessageItemProps {
  message: ChannelMessagesBasiFragementFragment & PrivateMessageItemInfosFragment;
  isChannel: boolean;
}

const MessageItem: FC<MessageItemProps> = props => {
  const {
    message: { message, author, createdAt, sender, isMine },
    isChannel,
  } = props;

  const user = isChannel ? author : sender;

  const name = getUserName(user);

  const classes = useStyle();
  return (
    <div className={classnames(classes.containerMessageItem, isMine && classes.mine)}>
      <Avatar
        indicatorStatus={user.isConnected ? 'online' : 'offline'}
        size={54}
        userName={name}
        profil={user.profil ? user.profil : ''}
        isCircle={true}
      />
      <div className={classes.messageContainer}>
        <div className={classes.authorContainer}>
          <MessageItemSender userName={name} />
          <MessageItemText text={message} />
        </div>
        <TimeAgo className={classes.time} dateTime={new Date(createdAt)} />
      </div>
    </div>
  );
};

export default MessageItem;

import { Typography } from '@material-ui/core';
import { FC } from 'react';
import useStyle from './styles';

interface MessageItemSenderProps {
  userName: string;
}

const MessageItemSender: FC<MessageItemSenderProps> = props => {
  const { userName } = props;
  const classes = useStyle();
  return (
    <Typography className={classes.containerMessageItemSender} variant="h6" title={userName}>
      {userName}
    </Typography>
  );
};

export default MessageItemSender;

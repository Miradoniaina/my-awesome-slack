import { useReactiveVar } from '@apollo/client/react/hooks/useReactiveVar';
import { FC } from 'react';
import { globalSnackbarVar } from '../../config/reactiveVars';
import { Snackbar } from './Snackbar';

const GlobalSnackbar: FC = () => {
  const snackbar = useReactiveVar(globalSnackbarVar);

  const { open, message: messageText, duration, type, anchor } = snackbar;
  const closeSnackBar = () => {
    globalSnackbarVar({
      ...snackbar,
      open: false,
    });
  };

  return (
    <Snackbar
      onClose={closeSnackBar}
      message={messageText}
      open={open}
      anchorOrigin={anchor}
      type={type}
      autoHideDuration={duration || 3000}
    />
  );
};

export default GlobalSnackbar;

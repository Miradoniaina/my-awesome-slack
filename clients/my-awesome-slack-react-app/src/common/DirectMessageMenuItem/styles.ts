import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  containerDirectMessageMenuItem: {
    width: '100%',
    paddingLeft: 30,
  },
  indicator: {
    ...theme.containerStyles.flexRow,
    alignItems: 'center',
  },
  margin: {
    marginRight: 10,
  },
}));

import { Dialog, DialogTitle, List } from '@material-ui/core';
import { FC } from 'react';
import { UserBasicInfosFragment } from '../../generated/graphql';
import { ModalAddParticipantUserItem } from './ModalAddParticipantUserItem';

interface ModalAddParticipantsProps {
  isOpen: boolean;
  onClose: () => void;
  users: UserBasicInfosFragment[];
  memberUsers: UserBasicInfosFragment[];
  channelId: string;
}

const ModalAddParticipants: FC<ModalAddParticipantsProps> = props => {
  const { isOpen, onClose, users, memberUsers, channelId } = props;

  const memberUsersIds = memberUsers.map(({ id }) => +id);

  const participantCanBeAdded = users.filter(u => {
    return u.isMe || memberUsersIds.includes(+u.id) ? null : u;
  });

  return (
    <Dialog onClose={onClose} aria-labelledby="simple-dialog-title" open={isOpen}>
      <DialogTitle id="simple-dialog-title">Add members</DialogTitle>
      <List>
        {participantCanBeAdded.map(user => (
          <ModalAddParticipantUserItem
            channelId={channelId}
            isActive={true}
            user={user}
            onClose={onClose}
          />
        ))}
      </List>
    </Dialog>
  );
};

export default ModalAddParticipants;

import { IconButton, ListItem, ListItemAvatar, ListItemText, Tooltip } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { FC } from 'react';
import { globalSnackbarVar } from '../../../config/reactiveVars';
import {
  useAddChannelParticipantMutation,
  UserBasicInfosFragment,
} from '../../../generated/graphql';
import { updateCacheAfterAddingParticipant } from '../../../updateCache/channel/addParticipant';
import { getUserName } from '../../../utils/user.utils';
import { Avatar } from '../../Avatar';
import Loading from '../../Loading';
import useStyle from './styles';

interface ModalAddParticipantUserItemProps {
  user: UserBasicInfosFragment;
  isActive: boolean;
  channelId: string;
  onClose: () => void;
}

const ModalAddParticipantUserItem: FC<ModalAddParticipantUserItemProps> = props => {
  const { user, isActive, channelId, onClose } = props;
  const classes = useStyle();

  const [addChannelParticipant, { loading: addChannelParticipantLoading }] =
    useAddChannelParticipantMutation({
      update: updateCacheAfterAddingParticipant,
    });

  const addParticipant = () => {
    if (!addChannelParticipantLoading) {
      addChannelParticipant({
        variables: {
          channel_id: +channelId,
          member: user.id,
        },
      })
        .catch(err => {
          globalSnackbarVar({
            open: true,
            message: err.message,
            type: 'ERROR',
          });
        })
        .then(() => {
          onClose();
        });
    }
  };

  const isLoading = addChannelParticipantLoading;

  return (
    <ListItem button={true} selected={isActive}>
      <ListItemAvatar>
        <Avatar
          indicatorStatus={user.isConnected ? 'online' : 'offline'}
          className={classes.avatar}
          profil={user && user.profil ? `${user && user.profil}` : ''}
          userName={getUserName(user)}
        />
      </ListItemAvatar>
      <ListItemText primary={getUserName(user)} />
      {!isLoading && (
        <Tooltip title="Invite" enterDelay={200}>
          <IconButton className={classes.actionButton} onClick={addParticipant}>
            <Add color="primary" />
          </IconButton>
        </Tooltip>
      )}
      {isLoading && (
        <IconButton className={classes.actionButton}>
          <Loading size={35} />
        </IconButton>
      )}
    </ListItem>
  );
};

export default ModalAddParticipantUserItem;

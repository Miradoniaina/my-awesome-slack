import { FC, useEffect, useRef } from 'react';
import {
  ChannelMessagesBasicInfosFragment,
  PrivateMessageItemInfosFragment,
} from '../../generated/graphql';
import { MessageItem } from '../MessageItem';
import useStyle from './styles';

interface ListMessageProps {
  isChannel: boolean;
  data: (PrivateMessageItemInfosFragment | ChannelMessagesBasicInfosFragment)[] | null | undefined;
}

const ListMessage: FC<ListMessageProps> = props => {
  const { data, isChannel } = props;
  const classes = useStyle();
  const divRef = useRef<HTMLDivElement>(null);

  const messages = data;

  const messageLength = messages && messages.length;

  useEffect(() => {
    setTimeout(() => {
      if (divRef.current) {
        divRef.current.scrollTo({
          top: divRef.current.scrollHeight,
          behavior: 'smooth',
        });
      }
    }, 1);
  }, [messageLength]);

  return (
    <div ref={divRef} className={classes.containerListMessage}>
      {messages &&
        messages.map(
          (m: any) =>
            m && (
              <div className={classes.messages}>
                <MessageItem message={m} isChannel={isChannel} />
              </div>
            )
        )}
    </div>
  );
};

export default ListMessage;

import './App.css';
import MainApp from './components/MainApp';

const App = () => {
  return (
    <div className="App">
      <MainApp />
    </div>
  );
};

export default App;

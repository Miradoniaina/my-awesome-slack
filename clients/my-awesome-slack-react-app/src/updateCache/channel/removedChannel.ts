import { DataProxy, MutationUpdaterFn } from '@apollo/client';
import { History } from 'history';
import { RemoveChannelMemberMutation } from '../../generated/graphql';
import { getChannelMembersInCache } from './getChannelMemberInCache';
import { getChannelsInCache } from './getChannelsInCache';

export const updateCacheAfterRemovingParticipant: MutationUpdaterFn<RemoveChannelMemberMutation> = (
  cache,
  { data },
  history?: History
) => {
  if (data?.removeChannelParticipant) {
    const { removeChannelParticipant } = data;
    const {
      id,
      member: { id: userId },
    } = removeChannelParticipant;
    removeUserCurrentUserFrontChannel(
      cache,
      {
        channelId: +id,
        userId: +userId,
      },
      history
    );
  }
};

export const removeUserCurrentUserFrontChannel = (
  cache: DataProxy,
  args: { channelId: number; userId: number },
  history?: History
) => {
  const { dataCache, queryVariable } = getChannelsInCache(cache);

  const { channelId, userId } = args;

  removeUserInMembers(cache, args);

  if (dataCache && dataCache.me && dataCache.me.channels) {
    const { channels, id: currentUserId } = dataCache.me;

    if (+userId !== +currentUserId) {
      return;
    }

    if (channels && channels.some(ch => +ch.id === +channelId)) {
      const newChannels = channels.filter(({ id }) => +id !== +channelId);
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          me: {
            ...dataCache.me,
            channels: newChannels,
          },
        },
      });
    }
    // TODO: check if current channel
    history?.replace('/');
  }
};

const removeUserInMembers = (cache: DataProxy, args: { channelId: number; userId: number }) => {
  const { channelId, userId } = args;
  const { dataCache, queryVariable } = getChannelMembersInCache(cache, channelId);

  if (dataCache?.channelMembers) {
    const { members } = dataCache.channelMembers;

    if (members && members.some(m => m && +m.id === +userId)) {
      const newMembers = members.filter(m => m && +m.id !== +userId);
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          channelMembers: {
            ...dataCache.channelMembers,
            members: newMembers,
          },
        },
      });
    }
  }
};

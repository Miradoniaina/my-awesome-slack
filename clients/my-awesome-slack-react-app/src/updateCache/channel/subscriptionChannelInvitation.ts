import { BaseSubscriptionOptions } from '@apollo/client';
import { SubscribeChannelInvitationSubscription } from '../../generated/graphql';
import { updateCacheAfterAddingChannel } from './addChannel';

export const updateCacheAfterSubscribsriptionChannelInvitation: BaseSubscriptionOptions<SubscribeChannelInvitationSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.userInviteChannel) {
      const { userInviteChannel } = subscriptionData.data;
      const { id } = userInviteChannel;

      if (id) {
        updateCacheAfterAddingChannel(client as any, {
          data: { createChannelUser: userInviteChannel },
        });
      }
    }
  };

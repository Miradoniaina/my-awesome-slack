import { BaseSubscriptionOptions } from '@apollo/client';
import { SubscribeToChannelMessageAddedSubscription } from '../../generated/graphql';
import { updateCacheAfterAddingChannelMessage } from './addChannelMessage';

export const updateCacheAfterSubscribsriptionMessageAdded: BaseSubscriptionOptions<SubscribeToChannelMessageAddedSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.addChannelMessage) {
      const { addChannelMessage } = subscriptionData.data;
      const { channelId } = addChannelMessage;
      if (channelId) {
        updateCacheAfterAddingChannelMessage(client as any, {
          data: { sendChannelMessage: addChannelMessage },
        });
      }
    }
  };

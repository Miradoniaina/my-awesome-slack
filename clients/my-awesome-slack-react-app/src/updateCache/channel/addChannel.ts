import { DataProxy, MutationUpdaterFn } from '@apollo/client';
import { CreateChannelMutation } from '../../generated/graphql';
import { getChannelsInCache } from './getChannelsInCache';

export const updateCacheAfterAddingChannel: MutationUpdaterFn<CreateChannelMutation> = (
  cache,
  { data }
) => {
  if (data?.createChannelUser) {
    const { createChannelUser } = data;
    updateCacheCommon(cache, createChannelUser);
  }
};

const updateCacheCommon = (
  cache: DataProxy,
  addChannel: CreateChannelMutation['createChannelUser']
) => {
  if (!addChannel) {
    return;
  }

  const { id } = addChannel;

  const { dataCache, queryVariable } = getChannelsInCache(cache);

  if (dataCache && dataCache.me && dataCache.me.channels) {
    const { channels } = dataCache.me;

    if (!channels.some((ch: any) => ch.id === id)) {
      const newChannels = [addChannel, ...channels];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          me: {
            ...dataCache.me,
            channels: newChannels,
          },
        },
      });
    }
  }
};

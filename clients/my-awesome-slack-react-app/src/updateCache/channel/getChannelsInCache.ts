import { DataProxy } from '@apollo/client';
import { MeQuery } from '../../generated/graphql';
import { ME } from '../../gql/user/queries';

export const getChannelsInCache = (cache: DataProxy) => {
  const queryVariable: DataProxy.Query<any, MeQuery> = {
    query: ME,
  };
  return { dataCache: cache.readQuery<MeQuery>(queryVariable), queryVariable };
};

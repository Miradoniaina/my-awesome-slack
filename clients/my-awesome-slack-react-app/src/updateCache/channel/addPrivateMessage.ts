import { DataProxy, MutationUpdaterFn } from '@apollo/client';
import { SendPrivateMessageMutation } from '../../generated/graphql';
import { getPrivateMessageInCache } from './getPrivateMessageInCache';

export const updateCacheAfterAddingPrivateMessage: MutationUpdaterFn<SendPrivateMessageMutation> = (
  cache,
  { data }
) => {
  if (data?.sendPrivateMessage) {
    const { sendPrivateMessage } = data;
    updateCacheCommon(cache, sendPrivateMessage);
  }
};

export const updateCacheAfterReceivingPrivateMessage: MutationUpdaterFn<
  SendPrivateMessageMutation
> = (cache, { data }) => {
  if (data?.sendPrivateMessage) {
    const { sendPrivateMessage } = data;
    updateCacheCommon(cache, sendPrivateMessage, true);
  }
};

const updateCacheCommon = (
  cache: DataProxy,
  sendPrivateMessage: SendPrivateMessageMutation['sendPrivateMessage'],
  receivingSubscription?: boolean
) => {
  if (!sendPrivateMessage) {
    return;
  }

  const { id, receiver, sender } = sendPrivateMessage;

  const privateUserId = receivingSubscription ? +sender.id : +receiver.id;
  const { dataCache, queryVariable } = getPrivateMessageInCache(cache, privateUserId);

  if (
    dataCache &&
    dataCache.privateMessages &&
    dataCache.privateMessages.privateMessages &&
    sendPrivateMessage
  ) {
    const { privateMessages } = dataCache.privateMessages;

    if (!privateMessages.some((pm: any) => pm.id === id)) {
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          privateMessages: {
            ...dataCache.privateMessages,
            privateMessages: [...privateMessages, sendPrivateMessage],
          },
        },
      });
    }
  }
};

import { BaseSubscriptionOptions } from '@apollo/client';
import { SubscribeToPrivateMessageAddedSubscription } from '../../generated/graphql';
import { updateCacheAfterReceivingPrivateMessage } from './addPrivateMessage';

export const updateCacheAfterSubscribsriptionPrivateMessageAdded: BaseSubscriptionOptions<SubscribeToPrivateMessageAddedSubscription>['onSubscriptionData'] =
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.sendPrivateMessage) {
      const { sendPrivateMessage } = subscriptionData.data;
      const { id } = sendPrivateMessage;

      if (id) {
        updateCacheAfterReceivingPrivateMessage(client as any, {
          data: { sendPrivateMessage },
        });
      }
    }
  };

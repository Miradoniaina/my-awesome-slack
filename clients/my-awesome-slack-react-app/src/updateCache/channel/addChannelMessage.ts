import { ApolloCache, MutationUpdaterFn } from '@apollo/client';
import { SendChannelMessageMutation } from '../../generated/graphql';
import { getChannelMessagesInCacheFn } from './getChannelMessageInCache';

export const updateCacheAfterAddingChannelMessage: MutationUpdaterFn<SendChannelMessageMutation> = (
  cache,
  { data }
) => {
  if (data?.sendChannelMessage) {
    const { sendChannelMessage } = data;
    updateCacheCommon(cache, sendChannelMessage);
  }
};

const updateCacheCommon = (
  cache: ApolloCache<any>,
  addChannelMessage: SendChannelMessageMutation['sendChannelMessage']
) => {
  if (!addChannelMessage) {
    return;
  }
  const { channelId, id: messageId } = addChannelMessage;

  if (!channelId) {
    return;
  }

  const { dataCache, queryVariable } = getChannelMessagesInCacheFn(cache, channelId);

  if (dataCache?.channelMessages?.channelMessages) {
    const { channelMessages } = dataCache.channelMessages;

    if (!channelMessages.some(ms => (ms && ms.id) === messageId)) {
      const newMessages = [...channelMessages, addChannelMessage];
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          channelMessages: {
            ...dataCache.channelMessages,
            channelMessages: newMessages,
          },
        },
      });
    }
  }
};

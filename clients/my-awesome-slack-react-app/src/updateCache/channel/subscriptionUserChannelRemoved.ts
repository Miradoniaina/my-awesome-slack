import { BaseSubscriptionOptions } from '@apollo/client';
import { History } from 'history';
import { SubscribeToUserAddedRemovedInChannelSubscription } from '../../generated/graphql';
import { updateCacheSubscriptionAddMember } from './addParticipant';
import { removeUserCurrentUserFrontChannel } from './removedChannel';

export const updateCacheAfterSubscriptionUserChannelAddedRemoved =
  (
    history: History
  ): BaseSubscriptionOptions<SubscribeToUserAddedRemovedInChannelSubscription>['onSubscriptionData'] =>
  ({ client, subscriptionData }) => {
    if (subscriptionData.data?.userAddRemoveChannel) {
      const { userAddRemoveChannel } = subscriptionData.data;
      const { id, member, type } = userAddRemoveChannel;

      if (type === 'add') {
        updateCacheSubscriptionAddMember(client as any, userAddRemoveChannel as any);
      } else {
        removeUserCurrentUserFrontChannel(
          client as any,
          { channelId: +id, userId: +member.id },
          history
        );
      }
    }
  };

import { DataProxy } from '@apollo/client';
import { GetChannelMemberQuery, GetChannelMemberQueryVariables } from '../../generated/graphql';
import { GET_CHANNEL_MEMBERS } from '../../gql/channel/member/queries';

export const getChannelMembersInCache = (cache: DataProxy, channelId: number) => {
  const queryVariable: DataProxy.Query<GetChannelMemberQueryVariables, GetChannelMemberQuery> = {
    query: GET_CHANNEL_MEMBERS,
    variables: {
      channelId,
    },
  };
  return { dataCache: cache.readQuery<GetChannelMemberQuery>(queryVariable), queryVariable };
};

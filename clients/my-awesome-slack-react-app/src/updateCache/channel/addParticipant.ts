import { DataProxy, MutationUpdaterFn } from '@apollo/client';
import {
  AddChannelParticipantMutation,
  ChannelMemberMutationPayload,
} from '../../generated/graphql';
import { getChannelsParticipantInCache } from './getChannelParticipantsInCache';

export const updateCacheAfterAddingParticipant: MutationUpdaterFn<AddChannelParticipantMutation> = (
  cache,
  { data }
) => {
  if (data?.addChannelParticipant) {
    const { addChannelParticipant } = data;
    updateCacheCommon(cache, addChannelParticipant);
  }
};

const updateCacheCommon = (
  cache: DataProxy,
  addChannelParticipant: AddChannelParticipantMutation['addChannelParticipant']
) => {
  if (!addChannelParticipant) {
    return;
  }

  const { id, member } = addChannelParticipant;

  const { dataCache, queryVariable } = getChannelsParticipantInCache(cache, +id);

  if (
    dataCache &&
    dataCache.channelMembers &&
    dataCache.channelMembers.id &&
    dataCache.channelMembers.members
  ) {
    const { members } = dataCache.channelMembers;

    if (!members.some((ch: any) => ch.id === member.id)) {
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          channelMembers: {
            ...dataCache.channelMembers,
            members: [...members, addChannelParticipant.member],
          },
        },
      });
    }
  }
};

export const updateCacheSubscriptionAddMember = (
  cache: DataProxy,
  addChannelParticipant: ChannelMemberMutationPayload
) => {
  if (!addChannelParticipant) {
    return;
  }

  const { id, member } = addChannelParticipant;

  const { dataCache, queryVariable } = getChannelsParticipantInCache(cache, +id);

  if (
    dataCache &&
    dataCache.channelMembers &&
    dataCache.channelMembers.id &&
    dataCache.channelMembers.members
  ) {
    const { members } = dataCache.channelMembers;

    if (!members.some((ch: any) => ch.id === member.id)) {
      cache.writeQuery({
        ...queryVariable,
        data: {
          __typename: 'Query',
          channelMembers: {
            ...dataCache.channelMembers,
            members: [...members, addChannelParticipant.member],
          },
        },
      });
    }
  }
};

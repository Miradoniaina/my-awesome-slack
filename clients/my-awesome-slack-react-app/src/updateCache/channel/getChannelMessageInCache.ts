import { DataProxy } from '@apollo/client';
import { GetChannelMessagesQuery, GetChannelMessagesQueryVariables } from '../../generated/graphql';
import { GET_CHANNEL_MESSAGES } from '../../gql/channel/message/queries';

export const getChannelMessagesInCacheFn = (cache: DataProxy, id: string) => {
  const queryVariable: DataProxy.Query<GetChannelMessagesQueryVariables, GetChannelMessagesQuery> =
    {
      query: GET_CHANNEL_MESSAGES,
      variables: {
        input: {
          channelId: +id,
        },
      },
    };
  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};

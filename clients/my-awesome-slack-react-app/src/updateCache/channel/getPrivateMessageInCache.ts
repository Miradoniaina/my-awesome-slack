import { DataProxy } from '@apollo/client';
import { GetPrivateMessageQuery, GetPrivateMessageQueryVariables } from '../../generated/graphql';
import { GET_PRIVATE_MESSAGES } from '../../gql/privateMessage/queries';

export const getPrivateMessageInCache = (cache: DataProxy, id: number) => {
  const queryVariable: DataProxy.Query<GetPrivateMessageQueryVariables, GetPrivateMessageQuery> = {
    query: GET_PRIVATE_MESSAGES,
    variables: {
      input: {
        privateUser: id,
      },
    },
  };

  return { dataCache: cache.readQuery(queryVariable), queryVariable };
};

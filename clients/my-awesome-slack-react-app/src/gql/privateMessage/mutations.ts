import { gql } from '@apollo/client';
import { PRIVATE_MESSAGE_ITEM_FRAGMENT } from './fragment';

export const SEND_PRIVATE_MESSAGE = gql`
  mutation SendPrivateMessage($input: InputPrivateMessage!) {
    sendPrivateMessage(input: $input) {
      ...PrivateMessageItemInfos
    }
  }
  ${PRIVATE_MESSAGE_ITEM_FRAGMENT}
`;

import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';

export const PRIVATE_MESSAGE_BASIC_FRAGMENT = gql`
  fragment PrivateMessageBasicInfos on PrivateMessage {
    id
    message
    isMine
    createdAt
  }
`;

export const PRIVATE_MESSAGE_ITEM_FRAGMENT = gql`
  fragment PrivateMessageItemInfos on PrivateMessage {
    ...PrivateMessageBasicInfos
    sender {
      ...UserBasicInfos
    }
    receiver {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${PRIVATE_MESSAGE_BASIC_FRAGMENT}
`;

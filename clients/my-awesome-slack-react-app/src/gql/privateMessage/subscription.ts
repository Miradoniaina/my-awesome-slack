import { gql } from '@apollo/client';
import { PRIVATE_MESSAGE_ITEM_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_PRIVATE_MESSAGE_ADDED = gql`
  subscription SubscribeToPrivateMessageAdded {
    sendPrivateMessage {
      ...PrivateMessageItemInfos
    }
  }
  ${PRIVATE_MESSAGE_ITEM_FRAGMENT}
`;

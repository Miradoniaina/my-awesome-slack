import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { PRIVATE_MESSAGE_ITEM_FRAGMENT } from './fragment';

export const GET_PRIVATE_MESSAGES = gql`
  query GetPrivateMessage($input: InputPrivateMessages) {
    privateMessages(input: $input) {
      id
      privateUser {
        ...UserBasicInfos
      }
      privateMessages {
        ...PrivateMessageItemInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${PRIVATE_MESSAGE_ITEM_FRAGMENT}
`;

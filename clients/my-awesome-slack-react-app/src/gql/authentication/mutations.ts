import { gql } from '@apollo/client';

export const LOGIN = gql`
  mutation Login($input: InputLogin!) {
    login(input: $input) {
      accessToken
    }
  }
`;

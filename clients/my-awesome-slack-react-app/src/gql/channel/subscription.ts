import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';
import { CHANNEL_BASIC_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_CHANNEL_INVITATION = gql`
  subscription SubscribeChannelInvitation {
    userInviteChannel {
      ...ChannelBasicInfos
    }
  }
  ${CHANNEL_BASIC_FRAGMENT}
`;

export const SUBSCRIBE_TO_USER_ADDED_REMOVED_IN_CHANNEL = gql`
  subscription SubscribeToUserAddedRemovedInChannel {
    userAddRemoveChannel {
      id
      member {
        ...UserBasicInfos
      }
      type
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

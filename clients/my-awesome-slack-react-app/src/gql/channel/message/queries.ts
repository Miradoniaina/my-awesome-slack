import { gql } from '@apollo/client';
import { CHANNEL_MESSAGES_FRAGMENT } from './fragment';

export const GET_CHANNEL_MESSAGES = gql`
  query GetChannelMessages($input: InputChannelMessages) {
    channelMessages(input: $input) {
      id
      channelName
      channelMessages {
        ...ChannelMessagesBasiFragement
      }
    }
  }
  ${CHANNEL_MESSAGES_FRAGMENT}
`;

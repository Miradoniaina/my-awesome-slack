import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../../user/fragment';

export const CHANNEL_MESSAGES_BASIC_FRAGMENT = gql`
  fragment ChannelMessagesBasicInfos on ChannelMessage {
    id
    message
    channelId
    createdAt
    isMine
  }
`;

export const CHANNEL_MESSAGES_FRAGMENT = gql`
  fragment ChannelMessagesBasiFragement on ChannelMessage {
    ...ChannelMessagesBasicInfos
    author {
      ...UserBasicInfos
    }
  }
  ${CHANNEL_MESSAGES_BASIC_FRAGMENT}
  ${USER_BASIC_FRAGMENT}
`;

import { gql } from '@apollo/client';
import { CHANNEL_MESSAGES_FRAGMENT } from './fragment';

export const SUBSCRIBE_TO_CHANNEL_MESSAGE_ADDED = gql`
  subscription SubscribeToChannelMessageAdded {
    addChannelMessage {
      ...ChannelMessagesBasiFragement
    }
  }
  ${CHANNEL_MESSAGES_FRAGMENT}
`;

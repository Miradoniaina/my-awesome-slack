import { gql } from '@apollo/client';
import { CHANNEL_MESSAGES_FRAGMENT } from './fragment';

export const SEND_CHANNEL_MESSAGE = gql`
  mutation SendChannelMessage($input: InputSendChannelMessage!) {
    sendChannelMessage(input: $input) {
      ...ChannelMessagesBasiFragement
    }
  }
  ${CHANNEL_MESSAGES_FRAGMENT}
`;

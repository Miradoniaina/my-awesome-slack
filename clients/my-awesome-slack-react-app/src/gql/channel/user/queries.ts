import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../../user/fragment';

export const GET_ALL_USER = gql`
  query GetAllUser {
    allUsers {
      ...UserBasicInfos
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../user/fragment';

export const CHANNEL_BASIC_FRAGMENT = gql`
  fragment ChannelBasicInfos on Channel {
    id
    name
    private
    owner {
      ...UserBasicInfos
    }
    private
  }
  ${USER_BASIC_FRAGMENT}
`;

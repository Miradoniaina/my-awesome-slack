import { gql } from '@apollo/client';
import { CHANNEL_BASIC_FRAGMENT } from './fragment';

export const CREATE_CHANNEL = gql`
  mutation CreateChannel($input: InputCreateChannelUser!) {
    createChannelUser(input: $input) {
      ...ChannelBasicInfos
    }
  }
  ${CHANNEL_BASIC_FRAGMENT}
`;

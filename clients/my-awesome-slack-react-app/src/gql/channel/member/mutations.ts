import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../../user/fragment';

export const ADD_CHANNEL_MEMBERS = gql`
  mutation AddChannelParticipant($channel_id: Int, $member: String) {
    addChannelParticipant(channel_id: $channel_id, member: $member) {
      id
      member {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

export const REMOVE_CHANNEL_MEMBER = gql`
  mutation RemoveChannelMember($input: InputRemoveChannelParticipant!) {
    removeChannelParticipant(input: $input) {
      id
      member {
        id
      }
    }
  }
`;

import { gql } from '@apollo/client';
import { USER_BASIC_FRAGMENT } from '../../user/fragment';

export const GET_CHANNEL_MEMBERS = gql`
  query GetChannelMember($channelId: Int) {
    channelMembers(channelId: $channelId) {
      id
      isOwner
      members {
        ...UserBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
`;

import { gql } from '@apollo/client';

export const SUBSCRIBE_TO_USERS_LOGGED_STATUS = gql`
  subscription SubscribeToUserLogsStatus {
    userLogsStatus {
      id
      isConnected
    }
  }
`;

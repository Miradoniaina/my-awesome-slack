import { gql } from '@apollo/client';
import { CHANNEL_BASIC_FRAGMENT } from '../channel/fragment';
import { USER_BASIC_FRAGMENT } from './fragment';

export const ME = gql`
  query me {
    me {
      ...UserBasicInfos
      channels {
        ...ChannelBasicInfos
      }
    }
  }
  ${USER_BASIC_FRAGMENT}
  ${CHANNEL_BASIC_FRAGMENT}
`;

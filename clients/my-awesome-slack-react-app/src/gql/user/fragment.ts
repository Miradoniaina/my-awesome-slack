import { gql } from '@apollo/client';

export const USER_BASIC_FRAGMENT = gql`
  fragment UserBasicInfos on User {
    id
    firstName
    lastName
    userName
    phone
    profil
    isMe
    isConnected
  }
`;
